const express = require("express");
const cors = require("cors");
const cookieSession = require("cookie-session");

const app = express();

app.use(
  cors({
    origin: true,
    credentials: true,
    methods: "POST,GET,PUT,OPTIONS,DELETE",
  })
);

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.use(
  cookieSession({
    name: "app-session",
    keys: ["COOKIE_SECRET"], // should use as secret environment variable
    httpOnly: true,
  })
);

const db = require("./app/models");
const Role = db.role;
const Unit = db.unit;
const User = db.user;
const User_Roles = db.user_roles;
const LoaiPhuongTien = db.loaiphuongtienvantai;

// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and Resync Db");
//   initial();
// });

// db.sequelize
//   .query("SET FOREIGN_KEY_CHECKS = 0", { raw: true })
//   .then(function () {
//     db.sequelize.sync({ force: true }).then(function () {
//       console.log("Drop and Resync Db");
//       initial();
//     });
//   });

db.sequelize.sync();

function initial() {
  Role.create({
    id: 1,
    name: "admin",
    status: 1,
  });

  Role.create({
    id: 2,
    name: "user",
    status: 1,
  });

  Role.create({
    id: 3,
    name: "lanhdao",
    status: 1,
  });

  Unit.create({
    id: 1,
    ten_don_vi: "Quản trị hệ thống",
    ma_don_vi: "ADMIN",
    trang_thai: 0,
  });

  User.create({
    id: 1,
    ho_va_ten: "Administrator",
    username: "superadmin",
    email: "superadmin.agg@vnpt.vn",
    password: "$2a$08$.UawaZureaO21MDDxDhhV.QnFw.ySkcXCnasEHdOLIPR3hWbwVQ46", // Vnpt@2024
    trang_thai: 1,
    unitID: 1,
  });

  User_Roles.create({
    id: 1,
    userId: 1,
    roleId: 1,
  });
  LoaiPhuongTien.create({
    id: 1,
    ma_loai: "XETAIDUOI10TAN",
    ten_loai: "Xe tải dưới 10 tấn",
    trang_thai: 1,
  });

  LoaiPhuongTien.create({
    id: 2,
    ma_loai: "XETAITREN10TAN",
    ten_loai: "Xe tải trên 10 tấn",
    trang_thai: 1,
  });

  LoaiPhuongTien.create({
    id: 3,
    ma_loai: "XEHOPDONG",
    ten_loai: "Xe hợp đồng",
    trang_thai: 1,
  });
}

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to VNPT An Giang application." });
});

// routes
require("./app/routes/auth.routes")(app);
require("./app/routes/user.routes")(app);
require("./app/routes/unit.routes")(app);
require("./app/routes/phuongtienvantai.routes")(app);
require("./app/routes/taixe.routes")(app);
require("./app/routes/loaiphuongtien.routes")(app);
require("./app/routes/nccthietbigsht.routes")(app);
require("./app/routes/laydulieuthongke.routes")(app);
require("./app/routes/reportslaydulieu.routes")(app);
require("./app/routes/taikhoangsht.routes")(app);
require("./app/routes/dashboard.routes")(app);
require("./app/routes/logtool.routes")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
