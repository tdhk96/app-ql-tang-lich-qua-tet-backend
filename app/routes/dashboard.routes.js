const { authJwt } = require("../middleware");
const controller = require("../controllers/dashboard.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.get(
    "/api/dashboard/thongke-ptvt",
    [authJwt.verifyToken],
    controller.getThongKePTVT
  );

  app.get(
    "/api/dashboard/thongke-taixe",
    [authJwt.verifyToken],
    controller.getThongKeTaiXe
  );

  app.get(
    "/api/dashboard/thongke-ptvt-hethan-dangkiem",
    [authJwt.verifyToken],
    controller.getPhuongTienDenHanDangKiem
  );

  app.get(
    "/api/dashboard/thongke-ptvt-hethan-baohiem",
    [authJwt.verifyToken],
    controller.getPhuongTienDenHanBaoHiem
  );

  app.get(
    "/api/dashboard/thongke-ptvt-hethan-phuhieu",
    [authJwt.verifyToken],
    controller.getPhuongTienDenHanPhuHieu
  );
};
