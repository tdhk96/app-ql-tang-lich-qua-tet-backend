const { authJwt } = require("../middleware");
const controller = require("../controllers/laydulieuthongke.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.post(
    "/api/thongkebaocao/lay-tat-ca",
    [authJwt.verifyToken],
    controller.getTatCaDuLieuTKNCC
  );

  app.post(
    "/api/thongkebaocao/lay-excel-bc-vi-pham",
    [authJwt.verifyToken],
    controller.getExcelBaoCaoXeViPham
  );

  app.get(
    "/api/thongkebaocao/lay-tat-ca-chi-tiet",
    [authJwt.verifyToken],
    controller.getTatCaDuLieuChiTietNCC
  );

  app.post(
    "/api/thongkebaocao/lay-chi-tiet-tung-cot",
    [authJwt.verifyToken],
    controller.getChiTietCotTKNCC
  );

  app.post(
    "/api/thongkebaocao/lay-tat-ca-chi-tiet-tglxlt",
    [authJwt.verifyToken],
    controller.getTatCaChiTietTKNCC
  );

  app.post(
    "/api/thongkebaocao/bao-cao-chi-tiet-vi-pham-toc-do-ngay-den-ngay",
    [authJwt.verifyToken],
    controller.getBCChiTietViPhamTocDoTuNgayDenNgay
  );

  app.post(
    "/api/thongkebaocao/lay-tat-ca-chi-tiet-tong-hop",
    [authJwt.verifyToken],
    controller.getTatCaDuLieuChiTietTKNCC
  );

  app.post(
    "/api/thongkebaocao/bao-cao-chi-tiet-vi-pham-tu-ngay-den-ngay",
    [authJwt.verifyToken],
    controller.getBCViPhamTuNgayDenNgay
  );

  app.post(
    "/api/thongkebaocao/bao-cao-mat-tin-hieu",
    [authJwt.verifyToken],
    controller.getBCMatTinHieu
  );
};
