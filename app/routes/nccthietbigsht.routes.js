const { authJwt } = require("../middleware");
const controller = require("../controllers/nccthietbigsht.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.get(
    "/api/danh-muc/ncc-thiet-bi-gsht/lay-tat-ca",
    [authJwt.verifyToken],
    controller.getTatCaNCCThietBiGSHT
  );

  app.get(
    "/api/danh-muc/ncc-thiet-bi-gsht/lay-ncc-thiet-bi-gsht-dang-hoat-dong/:id",
    [authJwt.verifyToken],
    controller.getNCCThietBiGSHT_Enabled
  );

  app.post(
    "/api/danh-muc/ncc-thiet-bi-gsht/them",
    [authJwt.verifyToken],
    controller.postThemNCCThietBiGSHT
  );

  app.delete(
    "/api/danh-muc/ncc-thiet-bi-gsht/xoa/:id",
    [authJwt.verifyToken],
    controller.postXoaNCCThietBiGSHT
  );

  app.get(
    "/api/danh-muc/ncc-thiet-bi-gsht/lay-du-lieu/:id",
    [authJwt.verifyToken],
    controller.getThongTinNCCThietBiGSHT
  );

  app.put(
    "/api/danh-muc/ncc-thiet-bi-gsht/sua/:id",
    [authJwt.verifyToken],
    controller.putChinhSuaNCCThietBiGSHT
  );
};
