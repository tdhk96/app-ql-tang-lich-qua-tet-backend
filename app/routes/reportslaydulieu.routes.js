const { authJwt } = require("../middleware");
const controller = require("../controllers/reportslaydulieu.controller");

module.exports = function (app) {
    app.use(function (req, res, next) {
      res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
      next();
    });
  
    app.post(
      "/api/thongkebaocao/import-lay-du-lieu",
      // API dùng cho tool
      controller.getLayDuLieuThongKeBaoCao
    );
  
    app.get(
      "/api/thongkebaocao/lay-tat-ca-ncc",
      // API dùng cho tool
      controller.getTatCaNCCThietBiGSHT
    );

    app.post(
      "/api/thongkebaocao/save-log-tool",
      // API dùng cho tool
      controller.saveLogTool
    );

    app.post(
      "/api/thongkebaocao/import-lay-du-lieu-sub",
      // API dùng cho tool
      controller.getLayDuLieuThongKeBaoCao_Sub
    );

    app.post(
      "/api/thongkebaocao/save-log-sub-tool",
      // API dùng cho tool
      controller.saveLogSubTool
    );
  };