const { authJwt } = require("../middleware");
const controller = require("../controllers/taixe.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.get(
    "/api/danh-muc/tai-xe/lay-tat-ca",
    [authJwt.verifyToken],
    controller.getTatCaTaiXe
  );

  app.post(
    "/api/danh-muc/tai-xe/lay-tat-ca",
    [authJwt.verifyToken],
    controller.postLayTatCaTaiXe
  );

  app.get(
    "/api/danh-muc/tai-xe/lay-tai-xe-dang-hoat-dong/:id",
    [authJwt.verifyToken],
    controller.getTaiXe_Enabled
  );

  app.post(
    "/api/danh-muc/tai-xe/them",
    [authJwt.verifyToken],
    controller.postThemTaiXe
  );

  app.delete(
    "/api/danh-muc/tai-xe/xoa/:id",
    [authJwt.verifyToken],
    controller.postXoaTaiXe
  );

  app.get(
    "/api/danh-muc/tai-xe/lay-du-lieu/:id",
    [authJwt.verifyToken],
    controller.getThongTinTaiXe
  );

  app.put(
    "/api/danh-muc/tai-xe/sua/:id",
    [authJwt.verifyToken],
    controller.putChinhSuaTaiXe
  );

  app.post(
    "/api/danh-muc/tai-xe/lay-ds-phuong-tien-cung-tx",
    [authJwt.verifyToken],
    controller.postListPhuongTienByTaiXe
  );

  app.post(
    "/api/danh-muc/tai-xe/xoa-tx-theo-ptvt",
    [authJwt.verifyToken],
    controller.postDeletTaiXeGanPhuongTien
  );

  app.post(
    "/api/danh-muc/tai-xe/gan-tai-xe",
    [authJwt.verifyToken],
    controller.postGanTaiXe
  );
};
