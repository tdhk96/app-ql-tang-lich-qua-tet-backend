const { authJwt } = require("../middleware");
const controller = require("../controllers/user.controller");
const controllerAuth = require("../controllers/auth.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  // app.get("/api/danh-muc/all", controller.allAccess);

  // app.get(
  //   "/api/danh-muc/user",
  //   [authJwt.verifyToken],
  //   controller.userBoard
  // );

  // app.get(
  //   "/api/danh-muc/mod",
  //   [authJwt.verifyToken, authJwt.isLanhDaoOrAdmin],
  //   controller.moderatorBoard
  // );

  // app.get(
  //   "/api/danh-muc/admin",
  //   [authJwt.verifyToken, authJwt.isAdmin],
  //   controller.adminBoard
  // );

  app.get(
    "/api/danh-muc/nguoi-dung/lay-tat-ca",
    [authJwt.verifyToken],
    controller.getTatCaNguoiDung
  );

  app.get(
    "/api/danh-muc/nguoi-dung/lay-nguoi-dung-dang-hoat-dong/:id",
    [authJwt.verifyToken],
    controller.getNguoiDung_Enabled
  );

  app.post(
    "/api/danh-muc/nguoi-dung/them",
    [authJwt.verifyToken],
    controllerAuth.signup
  );

  app.delete(
    "/api/danh-muc/nguoi-dung/xoa/:id",
    [authJwt.verifyToken],
    controller.postXoaNguoiDung
  );

  app.get(
    "/api/danh-muc/nguoi-dung/lay-du-lieu/:id",
    [authJwt.verifyToken],
    controller.getThongTinNguoiDung
  );

  app.put(
    "/api/danh-muc/nguoi-dung/sua/:id",
    [authJwt.verifyToken],
    controller.putChinhSuaNguoiDung
  );

  app.put(
    "/api/danh-muc/nguoi-dung/cap-nhat-trang-thai/:id",
    [authJwt.verifyToken],
    controller.putCapNhatTrangThaiTaiKhoan
  );

  app.get(
    "/api/danh-muc/nguoi-dung/danh-sach-quyen-hoat-dong",
    [authJwt.verifyToken],
    controller.getPhanQuyen_Enabled
  );

  app.post(
    "/api/danh-muc/nguoi-dung/phan-quyen/:id",
    [authJwt.verifyToken],
    controller.postPhanQuyen
  );

  app.get(
    "/api/danh-muc/nguoi-dung/danh-sach-quyen-nguoi-dung/:id",
    [authJwt.verifyToken],
    controller.getListQuyenNguoiDung
  );
};
