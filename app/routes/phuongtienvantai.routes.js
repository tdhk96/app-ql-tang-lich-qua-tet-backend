const { authJwt } = require("../middleware");
const controller = require("../controllers/phuongtienvantai.controller");
var multer = require("multer");
var upload = multer({ dest: "uploads/" });

var typeFileUpload = upload.single("upload-image-ptvt");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.post(
    "/api/danh-muc/phuong-tien-van-tai/lay-tat-ca",
    [authJwt.verifyToken],
    controller.postLayTatCaPhuongTien
  );

  app.get(
    "/api/danh-muc/phuong-tien-van-tai/lay-phuong-tien-dang-hoat-dong",
    [authJwt.verifyToken],
    controller.getPhuongTien_Enabled
  );

  app.post(
    "/api/danh-muc/phuong-tien-van-tai/them",
    [authJwt.verifyToken],
    controller.postThemPhuongTien
  );

  // app.post(
  //   "/api/danh-muc/phuong-tien-van-tai/gan-tai-xe",
  //   [authJwt.verifyToken],
  //   controller.postGanTaiXe
  // );

  app.delete(
    "/api/danh-muc/phuong-tien-van-tai/xoa/:id",
    [authJwt.verifyToken],
    controller.postXoaPhuongTien
  );

  app.get(
    "/api/danh-muc/phuong-tien-van-tai/lay-du-lieu/:id",
    [authJwt.verifyToken],
    controller.getThongTinPhuongTien
  );

  app.post(
    "/api/danh-muc/phuong-tien-van-tai/xuat-excel-tt-phuong-tien/:id",
    [authJwt.verifyToken],
    controller.getExcelThongTinPhuongTien
  );

  app.post(
    "/api/danh-muc/phuong-tien-van-tai/xuat-hang-loat-ly-lich-phuong-tien",
    [authJwt.verifyToken],
    controller.postInHangLoatLyLichPhuongTien
  );

  app.put(
    "/api/danh-muc/phuong-tien-van-tai/sua/:id",
    [authJwt.verifyToken],
    controller.putChinhSuaPhuongTien
  );

  app.post(
    "/api/danh-muc/phuong-tien-van-tai/upload-image",
    upload.single("upload-image-ptvt"),
    (req, res) => {
      if (!req.file) {
        return res.status(400).send("No file uploaded.");
      }

      const base64String = req.file.buffer.toString("base64");
      console.log(base64String);
      // res.send(base64String);
    }
  );

  app.get(
    "/api/danh-muc/phuong-tien-van-tai/chua-gan-tai-khoan-gsht",
    [authJwt.verifyToken],
    controller.getLayTatCaPhuongTienChuaGanTaiKhoanGSHT
  );

  app.get(
    "/api/danh-muc/phuong-tien-van-tai/den-han-bao-hiem",
    [authJwt.verifyToken],
    controller.getLayTatCaPhuongTienDenHanBaoHiem
  );

  app.get(
    "/api/danh-muc/phuong-tien-van-tai/den-han-dang-kiem",
    [authJwt.verifyToken],
    controller.getLayTatCaPhuongTienDenHanDangKiem
  );

  app.get(
    "/api/danh-muc/phuong-tien-van-tai/den-han-phu-hieu",
    [authJwt.verifyToken],
    controller.getLayTatCaPhuongTienDenHanPhuHieu
  );

  app.post(
    "/api/danh-muc/phuong-tien-van-tai/rut-phu-hieu/them",
    [authJwt.verifyToken],
    controller.postRutPhuHieuPhuongTien
  );

  app.post(
    "/api/danh-muc/phuong-tien-van-tai/rut-phu-hieu/lay-tat-ca",
    [authJwt.verifyToken],
    controller.postLayTatCaPhuongTienRutPhuHieu
  );

  app.put(
    "/api/danh-muc/phuong-tien-van-tai/rut-phu-hieu/sua/:id",
    [authJwt.verifyToken],
    controller.putChinhSuaRutPhuHieu
  );

  app.post(
    "/api/danh-muc/phuong-tien-van-tai/rut-phu-hieu/xoa-va-phuc-hoi-phuong-tien",
    [authJwt.verifyToken],
    controller.postXoaRutPhuHieu
  );

};
