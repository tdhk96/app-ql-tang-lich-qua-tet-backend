const { authJwt } = require("../middleware");
const controller = require("../controllers/logtool.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.post(
    "/api/thongkebaocao/bao-cao-lich-su-quet-du-lieu",
    [authJwt.verifyToken],
    controller.getLayLichSuQuetDuLieu
  );

  app.post(
    "/api/log-tool/lay-danh-sach-tai-khoan-loi",
    // API dùng cho tool
    controller.getLayDanhSachTaiKhoanLoi
  );

  app.post(
    "/api/log-tool/lay-danh-sach-tai-khoan-loi-sub",
    // API dùng cho tool
    controller.getLayDanhSachTaiKhoanLoi_Sub
  );
};
