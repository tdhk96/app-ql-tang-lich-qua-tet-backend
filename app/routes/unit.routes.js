const { authJwt } = require("../middleware");
const controller = require("../controllers/unit.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.get(
    "/api/don-vi",
    [authJwt.verifyToken],
    controller.getDonVi_Enabled
  );

  app.get(
    "/api/don-vi/tat-ca",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getTatCaDonVi
  );

  app.post(
    "/api/don-vi",
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.postDonVi
  );
};
