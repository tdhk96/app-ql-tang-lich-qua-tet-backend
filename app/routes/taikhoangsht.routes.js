const { authJwt } = require("../middleware");
const controller = require("../controllers/taikhoangsht.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.get(
    "/api/thongkebaocao/get-accounts-by-id",
    // API dùng cho tool
    controller.getTKTheoNCCThietBiGSHT
  );

  app.get(
    "/api/thongkebaocao/get-all-accounts",
    [authJwt.verifyToken],
    controller.getTatCaTKNCCThietBiGSHT
  );

  app.post(
    "/api/danh-muc/tai-khoan-gsht/lay-tat-ca",
    [authJwt.verifyToken],
    controller.postLayTatCaTaiKhoanGSHT
  );

  app.post(
    "/api/danh-muc/tai-khoan-gsht/lay-phuong-tien-cung-tai-khoan",
    [authJwt.verifyToken],
    controller.getPhuongTienCungTaiKhoan
  );

  app.post(
    "/api/danh-muc/tai-khoan-gsht/cap-nhat-tai-khoan",
    [authJwt.verifyToken],
    controller.putCapNhatTaiKhoan
  );

  app.post(
    "/api/danh-muc/tai-khoan-gsht/them",
    [authJwt.verifyToken],
    controller.postThemTaiKhoanGSHT
  );

  app.put(
    "/api/danh-muc/tai-khoan-gsht/cap-nhat-trang-thai-tai-khoan/:id",
    [authJwt.verifyToken],
    controller.putCapNhatTrangThaiTaiKhoan
  );

  app.post(
    "/api/danh-muc/tai-khoan-gsht/tai-khoan-gsht-theo-phuong-tien",
    [authJwt.verifyToken],
    controller.postTaiKhoanGSHTTheoPhuongTien
  );

  app.get(
    "/api/danh-muc/tai-khoan-gsht/thong-ke",
    [authJwt.verifyToken],
    controller.getThongKeTaiKhoan
  );
};
