const { authJwt } = require("../middleware");
const controller = require("../controllers/loaiphuongtien.controller");

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept");
    next();
  });

  app.get(
    "/api/danh-muc/loai-phuong-tien/lay-tat-ca",
    [authJwt.verifyToken],
    controller.getTatCaLoaiPhuongTien
  );

  app.get(
    "/api/danh-muc/loai-phuong-tien/lay-loai-phuong-tien-dang-hoat-dong",
    [authJwt.verifyToken],
    controller.getLoaiPhuongTien_Enabled
  );

  app.post(
    "/api/danh-muc/loai-phuong-tien/them",
    [authJwt.verifyToken],
    controller.postThemLoaiPhuongTien
  );

  app.delete(
    "/api/danh-muc/loai-phuong-tien/xoa/:id",
    [authJwt.verifyToken],
    controller.postXoaLoaiPhuongTien
  );

  app.get(
    "/api/danh-muc/loai-phuong-tien/lay-du-lieu/:id",
    [authJwt.verifyToken],
    controller.getThongTinLoaiPhuongTien
  );

  app.put(
    "/api/danh-muc/loai-phuong-tien/sua/:id",
    [authJwt.verifyToken],
    controller.putChinhSuaLoaiPhuongTien
  );
};
