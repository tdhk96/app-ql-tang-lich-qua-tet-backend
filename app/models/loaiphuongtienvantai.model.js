module.exports = (sequelize, Sequelize) => {
  const loaiPTVT = sequelize.define(
    "loaiphuongtienvantai",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      ma_loai: {
        type: Sequelize.STRING,
      },
      ten_loai: {
        type: Sequelize.STRING,
      },
      trang_thai: {
        type: Sequelize.STRING,
      },
    },
    {
      tableName: "loaiphuongtienvantai",
    }
  );

  return loaiPTVT;
};
