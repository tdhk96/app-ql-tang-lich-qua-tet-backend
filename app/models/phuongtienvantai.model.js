module.exports = (sequelize, Sequelize) => {
  const PhuongTienVanTai = sequelize.define(
    "phuongtienvantai",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      so_tv: {
        type: Sequelize.STRING,
      },
      hovaten_nguoidangky: {
        type: Sequelize.STRING,
      },
      hovaten_chuhientai: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      ngay_gia_nhap: {
        type: Sequelize.DATEONLY,
        allowNull: true,
      },
      namsinh_chuhientai: {
        type: Sequelize.DATEONLY,
        allowNull: true,
      },
      cccd_chuhientai: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      ngaycap_chuhientai: {
        type: Sequelize.DATEONLY,
      },
      noicap_chuhientai: {
        type: Sequelize.STRING,
      },
      diachi_dangky: {
        type: Sequelize.STRING,
      },
      diachi_hientai: {
        type: Sequelize.STRING,
      },
      sodienthoai_chuhientai: {
        type: Sequelize.STRING,
      },
      hddv: {
        type: Sequelize.DATEONLY,
      },
      bien_so: {
        type: Sequelize.STRING,
      },
      so_ghe: {
        type: Sequelize.INTEGER,
      },
      trong_tai: {
        type: Sequelize.FLOAT,
      },
      nhan_hieu: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      nam_san_xuat: {
        type: Sequelize.DATEONLY,
      },
      nien_han: {
        type: Sequelize.DATEONLY,
      },
      noi_san_xuat: {
        type: Sequelize.STRING,
      },
      so_khung: {
        type: Sequelize.STRING,
      },
      so_may: {
        type: Sequelize.STRING,
      },
      co_lop: {
        type: Sequelize.STRING,
      },
      tri_gia: {
        type: Sequelize.FLOAT,
      },

      dang_kiem: {
        type: Sequelize.DATEONLY,
      },
      bao_hiem: {
        type: Sequelize.DATEONLY,
      },
      so_phu_hieu: {
        type: Sequelize.STRING,
      },
      ngay_cap_phu_hieu: {
        type: Sequelize.DATEONLY,
      },
      ngay_het_han_phu_hieu: {
        type: Sequelize.DATEONLY,
      },
      trang_thai: {
        type: Sequelize.STRING,
      },
      ghi_chu: {
        type: Sequelize.STRING,
        allowNull: true
      },
      rut_ptvtId: {
        type: Sequelize.STRING,
        allowNull: true
      },
      taixeId: {
        type: Sequelize.STRING,
        allowNull: true
      },
    },
    {
      tableName: "phuongtienvantai",
    }
  );

  return PhuongTienVanTai;
};
