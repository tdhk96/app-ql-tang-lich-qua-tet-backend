module.exports = (sequelize, Sequelize) => {
  const taikhoangsht = sequelize.define(
    "taikhoangsht",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      tai_khoan: {
        type: Sequelize.STRING,
      },
      mat_khau: {
        type: Sequelize.STRING,
      },
      ngay_lap_thiet_bi: {
        type: Sequelize.STRING,
      },
      trang_thai: {
        type: Sequelize.STRING,
      },
      nccthietbigshtId: {
        type: Sequelize.INTEGER,
      },
    },
    {
      tableName: "taikhoangsht",
    }
  );

  return taikhoangsht;
};
