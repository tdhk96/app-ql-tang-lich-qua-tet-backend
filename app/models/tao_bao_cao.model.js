module.exports = (sequelize, Sequelize) => {
    const tao_bao_cao = sequelize.define(
      "report",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        tg_lay_bao_cao: {
          type: Sequelize.DATE,
        },
        tg_bat_dau: {
          type: Sequelize.DATE,
        },
        tg_ket_thuc: {
          type: Sequelize.DATE,
        },
        vipham_laixelientuc: {
          type: Sequelize.INTEGER,
        },
        vipham_tocdo_5den10: {
          type: Sequelize.INTEGER,
        },
        vipham_tocdo_10den20: {
          type: Sequelize.INTEGER,
        },
        vipham_tocdo_20den35: {
          type: Sequelize.INTEGER,
        },
        vipham_tocdo_tren35: {
          type: Sequelize.INTEGER,
        },
        bien_so_xe: {
          type: Sequelize.STRING,
        },
        nccthietbigshtId: {
          type: Sequelize.INTEGER,
        },
        so_km: {
          type: Sequelize.FLOAT,
        },
      },
      {
        tableName: "report",
      }
    );
  
    return tao_bao_cao;
  };
  