module.exports = (sequelize, Sequelize) => {
  const rut_ptvt = sequelize.define(
    "rut_ptvt",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      ngay_rut: {
        type: Sequelize.DATEONLY,
      },
      quyet_dinh_rut: {
        type: Sequelize.STRING,
      },

      ly_do_rut: {
        type: Sequelize.STRING,
      },
      ghi_chu: {
        type: Sequelize.STRING,
      },
    },
    {
      tableName: "rut_ptvt",
    }
  );

  return rut_ptvt;
};
