module.exports = (sequelize, Sequelize) => {
    const tao_bao_cao_sub = sequelize.define(
      "sub_report",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        tg_lay_bao_cao: {
          type: Sequelize.DATE,
        },
        tg_bat_dau: {
          type: Sequelize.DATE,
        },
        tg_ket_thuc: {
          type: Sequelize.DATE,
        },
        bien_so_xe: {
          type: Sequelize.STRING,
        },        
        so_km: {
          type: Sequelize.FLOAT,
        },
        nccthietbigshtId: {
          type: Sequelize.INTEGER,
        },
      },
      {
        tableName: "sub_report",
      }
    );
  
    return tao_bao_cao_sub;
  };
  