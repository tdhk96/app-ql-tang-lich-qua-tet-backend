module.exports = (sequelize, Sequelize) => {
    const chi_tiet_bctk = sequelize.define(
      "chi_tiet_bctk",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        bien_so_xe: {
          type: Sequelize.STRING,
        },
        ho_ten_lai_xe: {
          type: Sequelize.STRING,
        },
        tg_bat_dau: {
          type: Sequelize.DATE,
        },
        dia_diem_bat_dau: {
          type: Sequelize.STRING,
        },
        tg_ket_thuc: {
          type: Sequelize.DATE,
        },
        dia_diem_ket_thuc: {
          type: Sequelize.STRING,
        },
        tg_lai_xe: {
          type: Sequelize.STRING,
        },
        so_km: {
          type: Sequelize.STRING,
        },
        nccthietbigshtId: {
          type: Sequelize.INTEGER,
        },
      },
      {
        tableName: "chi_tiet_bctk",
      }
    );
  
    return chi_tiet_bctk;
  };
  