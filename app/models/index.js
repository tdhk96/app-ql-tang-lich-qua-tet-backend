const config = require("../config/db.config.js");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(config.DB, config.USER, config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  pool: {
    max: config.pool.max,
    min: config.pool.min,
    acquire: config.pool.acquire,
    idle: config.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.refreshToken = require("../models/refreshToken.model.js")(
  sequelize,
  Sequelize
);
db.unit = require("./unit.model.js")(sequelize, Sequelize);
db.phuongtienvantai = require("./phuongtienvantai.model.js")(
  sequelize,
  Sequelize
);
db.taixe = require("./taixe.model.js")(sequelize, Sequelize);
db.loaiphuongtienvantai = require("./loaiphuongtienvantai.model.js")(
  sequelize,
  Sequelize
);
db.nccthietbigsht = require("./nccthietbigsht.model.js")(sequelize, Sequelize);
db.rut_ptvt = require("./rut_ptvt.model.js")(sequelize, Sequelize);
db.taikhoangsht = require("./taikhoangsht.model.js")(sequelize, Sequelize);

db.tao_bao_cao = require("./tao_bao_cao.model.js")(sequelize, Sequelize);
db.chi_tiet_bctk = require("./chi_tiet_bctk.model.js")(sequelize, Sequelize);
db.chi_tiet_qua_toc_do_gioi_han = require("./chi_tiet_qua_toc_do_gioi_han.model.js")(sequelize, Sequelize);
db.bc_mat_tin_hieu = require("./bc_mat_tin_hieu.model.js")(
  sequelize,
  Sequelize
);

db.user_roles = require("./user_roles.model.js")(sequelize, Sequelize);
db.log_tool = require("./log_tool.model.js")(sequelize, Sequelize);
db.tao_bao_cao_sub = require("./tao_bao_cao_sub.model.js")(sequelize, Sequelize);
db.log_sub_tool = require("./log_sub_tool.model.js")(sequelize, Sequelize);

db.role.belongsToMany(db.user, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId",
});

db.user.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId",
});

db.refreshToken.belongsTo(db.user, {
  foreignKey: "userId",
  targetKey: "id",
});
db.user.hasOne(db.refreshToken, {
  foreignKey: "userId",
  targetKey: "id",
});

// Một ncc thiết bị có nhiều tài khoản
db.nccthietbigsht.hasMany(db.taikhoangsht);
db.taikhoangsht.belongsTo(db.nccthietbigsht);

// Một phương tiện có 1 tài khoản GSHT
db.phuongtienvantai.hasOne(db.taikhoangsht);
db.taikhoangsht.belongsTo(db.phuongtienvantai);

// Một phương tiện thuộc 1 loại phương tiện
db.loaiphuongtienvantai.hasOne(db.phuongtienvantai);
db.phuongtienvantai.belongsTo(db.loaiphuongtienvantai);

db.phuongtienvantai.hasOne(db.rut_ptvt);
db.rut_ptvt.belongsTo(db.phuongtienvantai);

db.nccthietbigsht.hasOne(db.tao_bao_cao);
db.tao_bao_cao.belongsTo(db.nccthietbigsht);

db.nccthietbigsht.hasOne(db.chi_tiet_bctk);
db.chi_tiet_bctk.belongsTo(db.nccthietbigsht);

db.nccthietbigsht.hasOne(db.bc_mat_tin_hieu);
db.bc_mat_tin_hieu.belongsTo(db.nccthietbigsht);

// Thiết lập quan hệ giữa tao_bao_cao và phuongtienvantai
db.tao_bao_cao.belongsTo(db.phuongtienvantai, {
  foreignKey: 'bien_so_xe',
  targetKey: 'bien_so',
  as: 'phuongtien'
});

module.exports = db;
