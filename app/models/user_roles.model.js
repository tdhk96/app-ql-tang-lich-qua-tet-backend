const User = require("../models/user.model");
const Role = require("../models/role.model");

module.exports = (sequelize, Sequelize) => {
  const UserRole = sequelize.define(
    "user_roles",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      userId: {
        type: Sequelize.INTEGER,
        // references: {
        //   model: User,
        //   key: "id",
        // },
      },
      roleId: {
        type: Sequelize.INTEGER,
        // references: {
        //   model: Role,
        //   key: "id",
        // },
      },
    },
    {
      tableName: "user_roles",
    }
  );
  return UserRole;
};
