module.exports = (sequelize, Sequelize) => {
    const bc_mat_tin_hieu = sequelize.define(
      "bc_mat_tin_hieu",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        bien_so_xe: {
          type: Sequelize.STRING,
        },
        tg_bd_mat_tin_hieu: {
          type: Sequelize.DATE,
        },
        dia_diem_bd_mat_tin_hieu: {
          type: Sequelize.STRING,
        },
        tg_kt_mat_tin_hieu: {
          type: Sequelize.DATE,
        },
        dia_diem_kt_mat_tin_hieu: {
          type: Sequelize.STRING,
        },
        tong_tg_mat_tin_hieu: {
          type: Sequelize.STRING,
        },
        nccthietbigshtId: {
          type: Sequelize.INTEGER,
        },
      },
      {
        tableName: "bc_mat_tin_hieu",
      }
    );
  
    return bc_mat_tin_hieu;
  };
  