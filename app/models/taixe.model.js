module.exports = (sequelize, Sequelize) => {
  const TaiXe = sequelize.define(
    "taixe",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      ho_va_ten: {
        type: Sequelize.STRING,
      },
      nam_sinh: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      cccd: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      ngay_cap: {
        type: Sequelize.DATEONLY,
        allowNull: true,
      },
      noi_cap: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      so_bang_lai: {
        type: Sequelize.STRING,
      },
      thoi_han_gplx: {
        type: Sequelize.DATEONLY,
      },
      hang: {
        type: Sequelize.STRING,
      },
      dia_chi_lai_xe: {
        type: Sequelize.STRING,
      },
      noi_kham: {
        type: Sequelize.STRING,
      },
      ngay_kham_sk: {
        type: Sequelize.STRING,
      },
      so_dien_thoai: {
        type: Sequelize.STRING,
      },
      ngay_tap_huan: {
        type: Sequelize.DATEONLY,
      },
      ngay_het_han_tap_huan: {
        type: Sequelize.DATEONLY,
      },
      so_tap_huan: {
        type: Sequelize.STRING,
      },
      trang_thai: {
        type: Sequelize.STRING,
      },
      ghi_chu: {
        type: Sequelize.STRING,
      },
    },
    {
      tableName: "taixe",
    }
  );

  return TaiXe;
};
