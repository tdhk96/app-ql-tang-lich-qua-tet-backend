module.exports = (sequelize, Sequelize) => {
    const log_tool = sequelize.define(
      "log_tool",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        tg_bat_dau: {
          type: Sequelize.DATE,
        },
        tg_ket_thuc: {
          type: Sequelize.DATE,
        },
        tai_khoan: {
          type: Sequelize.STRING,
        },
        nccthietbigshtId: {
          type: Sequelize.INTEGER,
        },
        trang_thai: {
          type: Sequelize.INTEGER,
        },
        message: {
          type: Sequelize.STRING,
        },
      },
      {
        tableName: "log_tool",
      }
    );
  
    return log_tool;
  };
  