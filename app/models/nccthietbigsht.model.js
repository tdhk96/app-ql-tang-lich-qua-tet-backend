module.exports = (sequelize, Sequelize) => {
    const nccthietbigsht = sequelize.define(
      "nccthietbigsht",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        website: {
          type: Sequelize.STRING,
        },
        ten_ncc: {
          type: Sequelize.STRING,
        },
      },
      {
        tableName: "nccthietbigsht",
      }
    );
  
    return nccthietbigsht;
  };
  