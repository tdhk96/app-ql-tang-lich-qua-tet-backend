module.exports = (sequelize, Sequelize) => {
    const Unit = sequelize.define("units", {
      ten_don_vi: {
        type: Sequelize.STRING,
      },
      ma_don_vi: {
        type: Sequelize.STRING
      },
      trang_thai: {
        type: Sequelize.STRING
      },
      mo_ta: {
        type: Sequelize.STRING
      }
    });
  
    return Unit;
  };