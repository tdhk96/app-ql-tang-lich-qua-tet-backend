module.exports = (sequelize, Sequelize) => {
    const chi_tiet_qua_toc_do_gioi_han = sequelize.define(
      "chi_tiet_qua_toc_do_gioi_han",
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true,
        },
        bien_so_xe: {
          type: Sequelize.STRING,
        },
        ho_ten_lai_xe: {
          type: Sequelize.STRING,
        },
        thoi_diem: {
          type: Sequelize.DATE,
        },
        dia_diem: {
          type: Sequelize.STRING,
        },
        toc_do_trung_binh: {
          type: Sequelize.FLOAT,
        },
        toc_do_gioi_han: {
          type: Sequelize.FLOAT,
        },
        nccthietbigshtId: {
          type: Sequelize.INTEGER,
        },
      },
      {
        tableName: "chi_tiet_qua_toc_do_gioi_han",
      }
    );
  
    return chi_tiet_qua_toc_do_gioi_han;
  };
  