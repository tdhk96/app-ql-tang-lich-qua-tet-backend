const { phuongtienvantai, sequelize, rut_ptvt } = require("../models");
var fs = require("fs");
// const config = require("../config/db.config.js");
// const Sequelize = require("sequelize");

// const sequelizeV2 = new Sequelize(config.DB, config.USER, config.PASSWORD, {
//   host: config.HOST,
//   dialect: config.dialect,
//   pool: {
//     max: config.pool.max,
//     min: config.pool.min,
//     acquire: config.pool.acquire,
//     idle: config.pool.idle,
//   },
// });

// Lấy tất cả các phương tiện
exports.postLayTatCaPhuongTien = async (req, res) => {
  const bien_so_xe_filter = req.body.bien_so_xe_filter
    ? req.body.bien_so_xe_filter
    : "%";
  const loai_phuong_tien_filter = req.body.loai_phuong_tien_filter
    ? req.body.loai_phuong_tien_filter
    : "%";
  const trang_thai_filter = req.body.trang_thai_filter
    ? req.body.trang_thai_filter
    : "%";
  const ten_chu_xe_filter = req.body.ten_chu_xe_filter
    ? req.body.ten_chu_xe_filter
    : "%";

  const data = await sequelize.query(
    `
    SELECT a.*, b.ten_loai
    FROM phuongtienvantai a
    LEFT JOIN loaiphuongtienvantai b ON a.loaiphuongtienvantaiId = b.id
    WHERE a.bien_so like '%` +
      bien_so_xe_filter +
      `%' AND a.loaiphuongtienvantaiId like '%` +
      loai_phuong_tien_filter +
      `%' AND a.trang_thai like '%` +
      trang_thai_filter +
      `%' AND a.hovaten_chuhientai like '%` +
      ten_chu_xe_filter +
      `%'`
  );
  if (data) {
    res.send(data[0]);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Lấy các phương tiện đang hoạt động
exports.getPhuongTien_Enabled = (req, res) => {
  res.status(200).send("User Content.");
};

// Lấy thông tin phương tiện
exports.getThongTinPhuongTien = (req, res) => {
  phuongtienvantai
    .findByPk(req.params.id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

exports.getExcelThongTinPhuongTien = async (req, res) => {
  try {
    const phuongTien = await phuongtienvantai.findByPk(req.params.id);
    const year = req.body.year;

    if (!phuongTien) {
      res.status(404).send({
        message: "Không tìm thấy phương tiện vận tải với ID: " + req.params.id,
      });
      return;
    }

    if (!year) {
      res.status(400).send({
        message: "Vui lòng cung cấp năm.",
      });
      return;
    }

    // Định dạng lại biển số xe để loại bỏ dấu cách và chuyển thành chữ in hoa
    const bienSoFormatted = phuongTien.bien_so.replace(/\s/g, "").toUpperCase();

    // Truy vấn để nhóm và tính tổng so_km theo tháng của năm được cung cấp
    let baoCaoByMonth;
    if (year === 2024) {
      baoCaoByMonth = await sequelize.query(
        `
        SELECT
          DATE_FORMAT(tg_bat_dau, '%Y-%m') AS month,
          ROUND(SUM(so_km), 2) AS tong_so_km
        FROM (
          SELECT
            bien_so_xe,
            DATE(tg_bat_dau) AS tg_bat_dau,
            MAX(so_km) AS so_km
          FROM sub_report
          WHERE bien_so_xe = :bienSoFormatted
            AND YEAR(tg_bat_dau) = :year
            AND MONTH(tg_bat_dau) <= 7
          GROUP BY bien_so_xe, DATE(tg_bat_dau)
          UNION ALL
          SELECT
            bien_so_xe,
            DATE(tg_bat_dau) AS tg_bat_dau,
            MAX(so_km) AS so_km
          FROM report
          WHERE bien_so_xe = :bienSoFormatted
            AND YEAR(tg_bat_dau) = :year
            AND MONTH(tg_bat_dau) >= 8
          GROUP BY bien_so_xe, DATE(tg_bat_dau)
        ) AS daily
        GROUP BY DATE_FORMAT(tg_bat_dau, '%Y-%m')
        `,
        {
          replacements: { bienSoFormatted, year },
          type: sequelize.QueryTypes.SELECT,
        }
      );
    } else {
      baoCaoByMonth = await sequelize.query(
        `
        SELECT
          DATE_FORMAT(tg_bat_dau, '%Y-%m') AS month,
          ROUND(SUM(so_km), 2) AS tong_so_km
        FROM (
          SELECT
            bien_so_xe,
            DATE(tg_bat_dau) AS tg_bat_dau,
            MAX(so_km) AS so_km
          FROM report
          WHERE bien_so_xe = :bienSoFormatted
            AND YEAR(tg_bat_dau) = :year
          GROUP BY bien_so_xe, DATE(tg_bat_dau)
        ) AS daily
        GROUP BY DATE_FORMAT(tg_bat_dau, '%Y-%m')
        `,
        {
          replacements: { bienSoFormatted, year },
          type: sequelize.QueryTypes.SELECT,
        }
      );
    }

    // Khởi tạo báo cáo cho tất cả các tháng trong năm với giá trị mặc định là 0
    const allMonths = Array.from(
      { length: 12 },
      (_, i) => `${year}-${String(i + 1).padStart(2, "0")}`
    );

    let tongCongSoKm = 0; // Biến để lưu tổng cộng số km lũy kế của năm
    let tongCongSoKmLuyKe = 0; // Biến để lưu tổng cộng số km lũy kế cuối cùng

    const baoCaoWithAllMonths = allMonths.map((month) => {
      const found = baoCaoByMonth.find((item) => item.month === month);
      const monthKm = found ? found.tong_so_km : 0;
      tongCongSoKm += monthKm; // Cộng dồn số km của từng tháng vào tổng số km

      // Tính lũy kế số km
      tongCongSoKmLuyKe += monthKm;

      return {
        month,
        [`tong_so_km_thang${parseInt(month.split("-")[1])}_${year}`]: monthKm,
        [`tong_so_km_luyke_thang${parseInt(month.split("-")[1])}_${year}`]:
          parseFloat(tongCongSoKmLuyKe.toFixed(2)),
      };
    });

    const result = {
      phuongTien,
      baoCao: baoCaoWithAllMonths,
      tongCongSoKm: parseFloat(tongCongSoKm.toFixed(2)),
      tongCongSoKmLuyKe: parseFloat(tongCongSoKmLuyKe.toFixed(2)),
    };

    res.send(result);
  } catch (err) {
    res.status(500).send({
      message:
        err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên.",
    });
  }
};

exports.postInHangLoatLyLichPhuongTien = async (req, res) => {
  const listPhuongTien = req.body.listOfPhuongTien;

  const year = req.body.year;
  let listResult = [];
  for (let pt of listPhuongTien) {
    try {
      const phuongTien = await phuongtienvantai.findByPk(pt);
      if (!phuongTien) {
        res.status(404).send({
          message:
            "Không tìm thấy phương tiện vận tải với ID: " + req.params.id,
        });
        return;
      }

      if (!year) {
        res.status(400).send({
          message: "Vui lòng cung cấp năm.",
        });
        return;
      }

      // Định dạng lại biển số xe để loại bỏ dấu cách và chuyển thành chữ in hoa
      const bienSoFormatted = phuongTien.bien_so
        .replace(/\s/g, "")
        .toUpperCase();

      // Truy vấn để nhóm và tính tổng so_km theo tháng của năm được cung cấp
      let baoCaoByMonth;
      if (year === 2024) {
        baoCaoByMonth = await sequelize.query(
          `
        SELECT
          DATE_FORMAT(tg_bat_dau, '%Y-%m') AS month,
          ROUND(SUM(so_km), 2) AS tong_so_km
        FROM (
          SELECT
            bien_so_xe,
            DATE(tg_bat_dau) AS tg_bat_dau,
            MAX(so_km) AS so_km
          FROM sub_report
          WHERE bien_so_xe = :bienSoFormatted
            AND YEAR(tg_bat_dau) = :year
            AND MONTH(tg_bat_dau) <= 7
          GROUP BY bien_so_xe, DATE(tg_bat_dau)
          UNION ALL
          SELECT
            bien_so_xe,
            DATE(tg_bat_dau) AS tg_bat_dau,
            MAX(so_km) AS so_km
          FROM report
          WHERE bien_so_xe = :bienSoFormatted
            AND YEAR(tg_bat_dau) = :year
            AND MONTH(tg_bat_dau) >= 8
          GROUP BY bien_so_xe, DATE(tg_bat_dau)
        ) AS daily
        GROUP BY DATE_FORMAT(tg_bat_dau, '%Y-%m')
        `,
          {
            replacements: { bienSoFormatted, year },
            type: sequelize.QueryTypes.SELECT,
          }
        );
      } else {
        baoCaoByMonth = await sequelize.query(
          `
        SELECT
          DATE_FORMAT(tg_bat_dau, '%Y-%m') AS month,
          ROUND(SUM(so_km), 2) AS tong_so_km
        FROM (
          SELECT
            bien_so_xe,
            DATE(tg_bat_dau) AS tg_bat_dau,
            MAX(so_km) AS so_km
          FROM report
          WHERE bien_so_xe = :bienSoFormatted
            AND YEAR(tg_bat_dau) = :year
          GROUP BY bien_so_xe, DATE(tg_bat_dau)
        ) AS daily
        GROUP BY DATE_FORMAT(tg_bat_dau, '%Y-%m')
        `,
          {
            replacements: { bienSoFormatted, year },
            type: sequelize.QueryTypes.SELECT,
          }
        );
      }

      // Khởi tạo báo cáo cho tất cả các tháng trong năm với giá trị mặc định là 0
      const allMonths = Array.from(
        { length: 12 },
        (_, i) => `${year}-${String(i + 1).padStart(2, "0")}`
      );

      let tongCongSoKm = 0; // Biến để lưu tổng cộng số km lũy kế của năm
      let tongCongSoKmLuyKe = 0; // Biến để lưu tổng cộng số km lũy kế cuối cùng

      const baoCaoWithAllMonths = allMonths.map((month) => {
        const found = baoCaoByMonth.find((item) => item.month === month);
        const monthKm = found ? found.tong_so_km : 0;
        tongCongSoKm += monthKm; // Cộng dồn số km của từng tháng vào tổng số km

        // Tính lũy kế số km
        tongCongSoKmLuyKe += monthKm;

        return {
          month,
          [`tong_so_km_thang${parseInt(month.split("-")[1])}_${year}`]: monthKm,
          [`tong_so_km_luyke_thang${parseInt(month.split("-")[1])}_${year}`]:
            parseFloat(tongCongSoKmLuyKe.toFixed(2)),
        };
      });

      const result = {
        phuongTien,
        baoCao: baoCaoWithAllMonths,
        tongCongSoKm: parseFloat(tongCongSoKm.toFixed(2)),
        tongCongSoKmLuyKe: parseFloat(tongCongSoKmLuyKe.toFixed(2)),
      };

      listResult.push(result);
    } catch (err) {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên.",
      });
    }
  }

  if (listResult != []) {
    res.status(200).send(listResult);
  } else {
    res.status(500).send({
      message:
        err.message || "Không lấy được dữ liệu.",
    });
  }
};

// Thêm phương tiện mới
exports.postThemPhuongTien = async (req, res) => {
  try {
    const ptvc = await phuongtienvantai.create({
      so_tv: req.body.so_tv,
      hovaten_nguoidangky: req.body.hovaten_nguoidangky,
      hovaten_chuhientai: req.body.hovaten_chuhientai,
      ngay_gia_nhap: req.body.ngay_gia_nhap,
      namsinh_chuhientai: req.body.namsinh_chuhientai,
      cccd_chuhientai: req.body.cccd_chuhientai,
      ngaycap_chuhientai: req.body.ngaycap_chuhientai,
      noicap_chuhientai: req.body.noicap_chuhientai,
      diachi_dangky: req.body.diachi_dangky,
      diachi_hientai: req.body.diachi_hientai,
      sodienthoai_chuhientai: req.body.sodienthoai_chuhientai,
      hddv: req.body.hddv,
      bien_so: req.body.bien_so,
      so_ghe: req.body.so_ghe,
      trong_tai: req.body.trong_tai,
      nhan_hieu: req.body.nhan_hieu,
      nam_san_xuat: req.body.nam_san_xuat,
      nien_han: req.body.nien_han,
      noi_san_xuat: req.body.noi_san_xuat,
      so_khung: req.body.so_khung,
      so_may: req.body.so_may,
      co_lop: req.body.co_lop,
      tri_gia: req.body.tri_gia,
      dang_kiem: req.body.dang_kiem,
      bao_hiem: req.body.bao_hiem,
      so_phu_hieu: req.body.so_phu_hieu,
      ngay_cap_phu_hieu: req.body.ngay_cap_phu_hieu,
      ngay_het_han_phu_hieu: req.body.ngay_het_han_phu_hieu,
      trang_thai: req.body.trang_thai,
      ghi_chu: req.body.ghi_chu,
      taikhoangshtId: Number(req.body.taikhoangshtId),
      loaiphuongtienvantaiId: Number(req.body.loaiphuongtienvantaiId),
    });
    if (ptvc) {
      res
        .status(200)
        .send({ message: "Thêm phương tiện thành công!", ptvc: ptvc });
    } else {
      res.status(500).send({ message: error.message });
    }
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

// Sửa thông tin phương tiện
exports.putChinhSuaPhuongTien = (req, res) => {
  const id = req.params.id;

  phuongtienvantai
    .update(req.body, {
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Cập nhật dữ liệu thành công!",
        });
      } else {
        res.send({
          message: `Lỗi không tìm thấy dữ liệu!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Xóa phương tiện
exports.postXoaPhuongTien = (req, res) => {
  const id = req.params.id;

  phuongtienvantai
    .destroy({
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Đã xóa phương tiện!",
        });
      } else {
        res.send({
          message: `Có lỗi, vui lòng liên hệ quản trị viên!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Có lỗi, vui lòng liên hệ quản trị viên",
      });
    });
};

// Upload hình ảnh
exports.postUploadImage = async (req, res) => {
  const fs = require("fs");
  // read binary data of the file
  const binaryData = fs.readFileSync(req.file);
  // convert binary data to base64 string
  const base64String = new Buffer(binaryData).toString("base64");
  console.log(base64String);
  // Perform further processing or save the file as needed
  // res.status(200).json({ base64String });
};

// Lấy tất cả các phương tiện chưa gán tài khoản GSHT
exports.getLayTatCaPhuongTienChuaGanTaiKhoanGSHT = async (req, res) => {
  try {
    const data = await sequelize.query(
      `
      SELECT DISTINCT phuongtienvantai.*
      FROM phuongtienvantai
      LEFT JOIN taikhoangsht ON phuongtienvantai.id = taikhoangsht.phuongtienvantaiId
      WHERE taikhoangsht.phuongtienvantaiId IS NULL;
      `,
      { type: sequelize.QueryTypes.SELECT }
    );
    res.status(200).send(data);
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Lấy tất cả các phương tiện gần đến hạn bảo hiểm
exports.getLayTatCaPhuongTienDenHanBaoHiem = async (req, res) => {
  try {
    const data = await sequelize.query(
      `
      SELECT DISTINCT phuongtienvantai.*
      FROM phuongtienvantai
      WHERE bao_hiem BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 60 DAY);
      `,
      { type: sequelize.QueryTypes.SELECT }
    );
    res.status(200).send(data);
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Lấy tất cả các phương tiện gần đến hạn đăng kiểm
exports.getLayTatCaPhuongTienDenHanDangKiem = async (req, res) => {
  try {
    const data = await sequelize.query(
      `
      SELECT DISTINCT phuongtienvantai.*
      FROM phuongtienvantai
      WHERE dang_kiem BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 60 DAY);
      `,
      { type: sequelize.QueryTypes.SELECT }
    );
    res.status(200).send(data);
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Lấy tất cả các phương tiện gần đến hạn phù hiệu
exports.getLayTatCaPhuongTienDenHanPhuHieu = async (req, res) => {
  try {
    const data = await sequelize.query(
      `
      SELECT DISTINCT phuongtienvantai.*
      FROM phuongtienvantai
      WHERE ngay_het_han_phu_hieu BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 60 DAY);
      `,
      { type: sequelize.QueryTypes.SELECT }
    );
    res.status(200).send(data);
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

exports.postRutPhuHieuPhuongTien = async (req, res) => {
  const listPhuongTien = req.body.phuongtienvantaiId;
  let count = 0;
  for (let pt of listPhuongTien) {
    try {
      const transaction = await sequelize.transaction();
      // Thực hiện công việc đầu tiên: Thêm dữ liệu vào bảng rut_ptvt
      const rph = await rut_ptvt.create({
        ngay_rut: req.body.ngay_rut,
        quyet_dinh_rut: req.body.quyet_dinh_rut,
        ly_do_rut: req.body.ly_do_rut,
        ghi_chu: req.body.ghi_chu,
      });

      // Thực hiện công việc thứ hai: cập nhật trạng thái của phương tiện vận tải
      const ptvt_update = await phuongtienvantai.update(
        { trang_thai: "0", rut_ptvtId: rph.id },
        {
          where: { id: pt },
        }
      );

      if (rph && ptvt_update) {
        count++;
      }

      // Xác nhận giao dịch
      await transaction.commit();
    } catch (error) {
      // Nếu có lỗi, rollback giao dịch
      await transaction.rollback();
      res.status(500).send("Thực hiện không thành công");
    }
  }

  if (count > 0) {
    res.status(200).send({ message: "Rút phù hiệu thành công!" });
  } else {
    res
      .status(500)
      .send({ message: "Không có xe nào rút phù hiệu thành công" });
  }
};

exports.postLayTatCaPhuongTienRutPhuHieu = async (req, res) => {
  const bien_so_xe_filter = req.body.bien_so_xe_filter
    ? req.body.bien_so_xe_filter
    : "%";
  const loai_phuong_tien_filter = req.body.loai_phuong_tien_filter
    ? req.body.loai_phuong_tien_filter
    : "%";
  const quyet_dinh_rut_filter = req.body.quyet_dinh_rut_filter
    ? req.body.quyet_dinh_rut_filter
    : "%";
  const [data, metadata] = await sequelize.query(
    ` SELECT
        GROUP_CONCAT(a.id ) AS listPhuongTien,
        GROUP_CONCAT(  " ", a.bien_so ) AS listBSX,
        b.id,
        b.ngay_rut,
        b.ly_do_rut,
        b.quyet_dinh_rut,
        b.ghi_chu 
      FROM
        phuongtienvantai AS a
        RIGHT JOIN rut_ptvt AS b ON a.rut_ptvtId = b.id
        LEFT JOIN loaiphuongtienvantai AS c ON a.loaiphuongtienvantaiId = c.id
      WHERE b.quyet_dinh_rut like '%` +
      quyet_dinh_rut_filter +
      `%' AND c.id like '` +
      loai_phuong_tien_filter +
      `' AND a.bien_so LIKE '%` +
      bien_so_xe_filter +
      `%'
      GROUP BY
        a.rut_ptvtId,
        b.id,
        b.ngay_rut,
        b.ly_do_rut,
        b.quyet_dinh_rut,
        b.ghi_chu`
  );
  if (data) {
    res.status(200).send(data);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

exports.putChinhSuaRutPhuHieu = async (req, res) => {
  const id = req.params.id;
  const listPhuongTien = req.body.listPhuongTien;
  let count = 0;
  const [ptRutPhuHieu_Old, metadata] = await sequelize.query(
    `SELECT a.id FROM phuongtienvantai AS a WHERE a.rut_ptvtId = '` + id + `'`
  );

  let listPhuongTien_convert = listPhuongTien.map(Number);
  let ptvt_da_rut = ptRutPhuHieu_Old.filter((item) =>
    listPhuongTien_convert.includes(item.id)
  );
  let ptvt_update_active = ptRutPhuHieu_Old.filter(
    (item) => !listPhuongTien_convert.includes(item.id)
  );
  let ptvt_rut_new = listPhuongTien_convert.filter(
    (item) => !ptRutPhuHieu_Old.some((aItem) => aItem.id == item)
  );
  const transaction = await sequelize.transaction();

  try {
    const rph = await rut_ptvt.update(
      {
        ngay_rut: req.body.ngay_rut,
        quyet_dinh_rut: req.body.quyet_dinh_rut,
        ly_do_rut: req.body.ly_do_rut,
        ghi_chu: req.body.ghi_chu,
      },
      {
        where: { id: id },
      }
    );

    if (ptvt_update_active.length !== 0) {
      for (let kr of ptvt_update_active) {
        const ptvt_update = await phuongtienvantai.update(
          { trang_thai: "1", rut_ptvtId: null },
          { where: { id: kr.id } }
        );
      }
    }

    if (ptvt_rut_new.length !== 0) {
      for (let kr of ptvt_rut_new) {
        const ptvt_update = await phuongtienvantai.update(
          { trang_thai: "0", rut_ptvtId: id },
          { where: { id: kr } }
        );
      }
    }

    // Xác nhận giao dịch
    await transaction.commit();
    res
      .status(200)
      .send({ message: "Cập nhật thông tin rút phù hiệu thành công!" });
  } catch (error) {
    // Nếu có lỗi, rollback giao dịch
    await transaction.rollback();
    res.status(500).send("Thực hiện không thành công");
  }
};

exports.postXoaRutPhuHieu = async (req, res) => {
  const id = req.body.id;
  const [ptRutPhuHieu_Old, metadata] = await sequelize.query(
    `SELECT a.id FROM phuongtienvantai AS a WHERE a.rut_ptvtId = '` + id + `'`
  );
  const transaction = await sequelize.transaction();
  try {
    for (let pt of ptRutPhuHieu_Old) {
      const ptvt_update = await phuongtienvantai.update(
        { trang_thai: "1", rut_ptvtId: null },
        { where: { id: pt.id } }
      );
    }

    const rph = await rut_ptvt.destroy({
      where: { id: id },
    });

    // Xác nhận giao dịch
    await transaction.commit();
    res
      .status(200)
      .send({ message: "Cập nhật thông tin rút phù hiệu thành công!" });
  } catch (error) {
    // Nếu có lỗi, rollback giao dịch
    await transaction.rollback();
    res.status(500).send("Thực hiện không thành công");
  }
};
