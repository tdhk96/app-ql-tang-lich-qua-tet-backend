const { nccthietbigsht } = require("../models");
var fs = require("fs");

// Lấy tất cả các nhà cung cấp
exports.getTatCaNCCThietBiGSHT = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  nccthietbigsht
    .findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy các nhà cung cấp đang hoạt động
exports.getNCCThietBiGSHT_Enabled = (req, res) => {
  nccthietbigsht
    .findAll({ where: { trang_thai: "1" } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy thông tin nhà cung cấp
exports.getThongTinNCCThietBiGSHT = (req, res) => {
  nccthietbigsht
    .findByPk(req.params.id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Thêm nhà cung cấp mới
exports.postThemNCCThietBiGSHT = async (req, res) => {
  try {
    const tx = await nccthietbigsht.create({
      ten_ncc: req.body.ten_ncc,
      website: req.body.website,
    });
    if (tx) {
      res.status(200).send({ message: "Thêm nhà cung cấp thành công!", nccthietbigsht: tx });
    } else {
      res.status(500).send({ message: error.message });
    }
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

// Sửa thông tin nhà cung cấp
exports.putChinhSuaNCCThietBiGSHT = (req, res) => {
  const id = req.params.id;

  nccthietbigsht
    .update(req.body, {
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Cập nhật dữ liệu thành công!",
        });
      } else {
        res.send({
          message: `Lỗi không tìm thấy dữ liệu!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Xóa nhà cung cấp
exports.postXoaNCCThietBiGSHT = (req, res) => {
  const id = req.params.id;

  nccthietbigsht
    .destroy({
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Đã xóa nhà cung cấp!",
        });
      } else {
        res.send({
          message: `Có lỗi, vui lòng liên hệ quản trị viên!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Có lỗi, vui lòng liên hệ quản trị viên",
      });
    });
};
