const {
  taikhoangsht,
  nccthietbigsht,
  sequelize,
  phuongtienvantai,
} = require("../models");
const { Op } = require("sequelize");

exports.getTKTheoNCCThietBiGSHT = (req, res) => {
  const nccthietbigshtId = req.body.nccthietbigshtId;
  let condition = {
    trang_thai: "1", // Chỉ lấy những tài khoản có trang_thai là '1'
  };

  if (nccthietbigshtId) {
    condition.nccthietbigshtId = nccthietbigshtId;
  }

  taikhoangsht
    .findAll({
      where: condition,
      attributes: ["tai_khoan", "mat_khau", "nccthietbigshtId"],
      include: [
        {
          model: nccthietbigsht,
          as: "nccthietbigsht",
          attributes: ["id"],
        },
      ],
    })
    .then((data) => {
      // Chuyển đổi dữ liệu thành mảng các đối tượng và gộp các tài khoản trùng nhau
      let accountMap = {};

      for (let i = 0; i < data.length; i++) {
        const item = data[i];
        const key = `${item.tai_khoan}_${item.mat_khau}`;
        if (!accountMap[key]) {
          accountMap[key] = {
            tai_khoan: item.tai_khoan,
            mat_khau: item.mat_khau,
          };
        }
      }

      let result = Object.values(accountMap);

      res.status(200).send(result);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy tất cả các nhà cung cấp
exports.getTatCaTKNCCThietBiGSHT = (req, res) => {
  taikhoangsht
    .findAll({})
    .then((data) => {
      res.status(200).send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy tất cả các phương tiện
exports.postLayTatCaTaiKhoanGSHT = async (req, res) => {
  const tai_khoan_filter = req.body.tai_khoan_filter
    ? req.body.tai_khoan_filter
    : "%";
  const ncc_filter = req.body.ncc_filter ? req.body.ncc_filter : "%";
  const trang_thai_filter = req.body.trang_thai_filter
    ? req.body.trang_thai_filter
    : "%";

  const bien_so_xe_filter = req.body.bien_so_xe_filter
    ? req.body.bien_so_xe_filter
    : "%";

  const sqlStr = `
    SELECT a.*, b.ten_ncc, b.website, c.bien_so, c.hovaten_nguoidangky, c.hovaten_chuhientai, c.sodienthoai_chuhientai, c.loaiphuongtienvantaiid, c.id AS phuongtienid
    FROM taikhoangsht AS a
    LEFT JOIN nccthietbigsht AS b ON a.nccthietbigshtId = b.id
    LEFT JOIN phuongtienvantai AS c ON a.phuongtienvantaiId = c.id
    WHERE a.tai_khoan like '` +
      tai_khoan_filter +
      `' AND a.nccthietbigshtId like '` +
      ncc_filter +
      `' AND a.trang_thai like '` +
      trang_thai_filter +
      `' AND c.bien_so like '` +
      bien_so_xe_filter +
      `' AND c.trang_thai !='0'`;

  const [data] = await sequelize.query(sqlStr);
  if (data) {
    res.status(200).send(data);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Lấy các phương tiện có cùng tài khoản
exports.getPhuongTienCungTaiKhoan = async (req, res) => {
  const taikhoan = req.body.taikhoan;
  const matkhau = req.body.matkhau;
  const nccId = req.body.nccId;

  const data = await sequelize.query(
    `
    SELECT a.*, c.bien_so, c.hovaten_nguoidangky, c.hovaten_chuhientai, c.sodienthoai_chuhientai
    FROM taikhoangsht AS a
    LEFT JOIN phuongtienvantai AS c ON a.phuongtienvantaiId = c.id
    WHERE a.tai_khoan like '` +
      taikhoan +
      `' AND a.nccthietbigshtId like '` +
      nccId +
      `' AND a.mat_khau like '` +
      matkhau +
      `'`
  );
  if (data) {
    res.send(data[0]);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Cập nhật tài khoản
exports.putCapNhatTaiKhoan = async (req, res) => {
  const accinfor = req.body.accinfor;

  const listPT = req.body.listPT;
  let count = 0;

  for (let pt of listPT) {
    try {
      updateData = {
        id: accinfor.id,
        tai_khoan: accinfor.tai_khoan,
        mat_khau: accinfor.mat_khau,
        ngay_lap_thiet_bi: accinfor.ngay_lap_thiet_bi,
        phuongtienvantaiId: pt,
        nccthietbigshtId: accinfor.nccthietbigshtId,
        trang_thai: accinfor.trang_thai,
        ghi_chu: accinfor.ghi_chu,
      };

      const data = await sequelize.query(
        `
        UPDATE taikhoangsht
        SET tai_khoan = '` +
          accinfor.tai_khoan +
          `', mat_khau = '` +
          accinfor.mat_khau +
          `', ngay_lap_thiet_bi = '` +
          accinfor.ngay_lap_thiet_bi +
          `', nccthietbigshtId = ` +
          accinfor.nccthietbigshtId +
          `
        WHERE phuongtienvantaiId = ` +
          pt
      );
      if (data) {
        count++;
      }
    } catch (error) {
      console.error("Lỗi khi cập nhật tài khoản:", error);
      throw error;
    }
  }

  if (count > 0) {
    res.status(200).send({
      message:
        "Đã thành công cập nhật tài khoản cho " + count + " phương tiện!",
    });
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Cập nhật trạng thái
exports.putCapNhatTrangThaiTaiKhoan = async (req, res) => {
  const id = req.params.id;

  try {
    const data = await sequelize.query(
      `
      UPDATE taikhoangsht
      SET trang_thai = '` +
        req.body.trang_thai +
        `'
      WHERE id = ` +
        id
    );

    if (data) {
      return res
        .status(200)
        .send({ message: "Trạng thái tài khoản đã được cập nhật" });
    } else {
      return res
        .status(500)
        .send({ message: "Không tìm thấy tài khoản với ID này" });
    }
  } catch (error) {
    console.error("Lỗi khi cập nhật trạng thái tài khoản:", error);
    throw error;
  }
};

// Tìm tài khoản theo phương tiện
exports.postTaiKhoanGSHTTheoPhuongTien = async (req, res) => {
  const phuongtienvantaiId = req.body.ptvtId;
  const [data, metadata] = await sequelize.query(
    `
    SELECT *
    FROM taikhoangsht
    WHERE phuongtienvantaiId = ` +
      phuongtienvantaiId +
      ` LIMIT 1`
  );
  if (data) {
    res.status(200).send(data[0]);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

exports.postThemTaiKhoanGSHT = async (req, res) => {
  try {
    const tk = await taikhoangsht.create({
      tai_khoan: req.body.tai_khoan,
      mat_khau: req.body.mat_khau,
      ngay_lap_thiet_bi: req.body.ngay_lap_thiet_bi,
      phuongtienvantaiId: req.body.phuongtienvantaiId,
      nccthietbigshtId: req.body.nccthietbigshtId,
      trang_thai: req.body.trang_thai,
      ghi_chu: req.body.ghi_chu,
    });
    if (tk) {
      res.status(200).send({ message: "Thêm tài khoản GSHT thành công!" });
    } else {
      res.status(500).send({ message: error.message });
    }
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

exports.getThongKeTaiKhoan = async (req, res) => {
  const [data, metadata] = await sequelize.query(
    `SELECT 
      COUNT(CASE WHEN tk.trang_thai = '0' THEN 1 END) AS countTKNgungHoatDong,
      COUNT(CASE WHEN tk.tai_khoan IS NULL THEN 1 END) AS countKhongCoTK
      FROM 
          taikhoangsht tk
      LEFT JOIN 
          phuongtienvantai pt ON tk.phuongtienvantaiId = pt.id
      WHERE 
          pt.trang_thai != '0';`
  );

  if (data) {
    res.json(data[0]);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};
