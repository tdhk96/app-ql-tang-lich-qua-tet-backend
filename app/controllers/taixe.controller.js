const { taixe, sequelize } = require("../models");
var fs = require("fs");

// Lấy tất cả các tài xế
exports.getTatCaTaiXe = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  taixe
    .findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy tất cả các phương tiện
exports.postLayTatCaTaiXe = async (req, res) => {
  const ten_tai_xe_filter = req.body.ten_tai_xe_filter
    ? "%" + req.body.ten_tai_xe_filter + "%"
    : "%";
  const cccd_filter = req.body.cccd_filter
    ? "%" + req.body.cccd_filter + "%"
    : "%";
  const trang_thai_filter = req.body.trang_thai_filter
    ? req.body.trang_thai_filter
    : "%";

  const data = await sequelize.query(
    `
    SELECT a.*
    FROM taixe a
    WHERE a.ho_va_ten like '` +
      ten_tai_xe_filter +
      `' AND a.cccd like '` +
      cccd_filter +
      `' AND a.trang_thai like '` +
      trang_thai_filter +
      `'`
  );
  if (data) {
    res.send(data[0]);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Lấy các tài xế đang hoạt động
exports.getTaiXe_Enabled = (req, res) => {
  taixe
    .findAll({ where: { trang_thai: "1" } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy thông tin tài xế
exports.getThongTinTaiXe = (req, res) => {
  taixe
    .findByPk(req.params.id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Thêm tài xế mới
exports.postThemTaiXe = async (req, res) => {
  try {
    const tx = await taixe.create({
      ho_va_ten: req.body.ho_va_ten,
      nam_sinh: req.body.nam_sinh,
      cccd: req.body.cccd,
      ngay_cap: req.body.ngay_cap,
      noi_cap: req.body.noi_cap,
      so_bang_lai: req.body.so_bang_lai,
      thoi_han_gplx: req.body.thoi_han_gplx,
      hang: req.body.hang,
      dia_chi_lai_xe: req.body.dia_chi_lai_xe,
      noi_kham: req.body.noi_kham,
      ngay_kham_sk: req.body.ngay_kham_sk,
      so_dien_thoai: req.body.so_dien_thoai,
      ngay_tap_huan: req.body.ngay_tap_huan,
      ngay_het_han_tap_huan: req.body.ngay_het_han_tap_huan,
      so_tap_huan: req.body.so_tap_huan,
      trang_thai: req.body.trang_thai,
      ghi_chu: req.body.ghi_chu,
    });
    if (tx) {
      res.status(200).send({ message: "Thêm tài xế thành công!", taixe: tx });
    } else {
      res.status(500).send({ message: error.message });
    }
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

// Sửa thông tin tài xế
exports.putChinhSuaTaiXe = (req, res) => {
  const id = req.params.id;

  taixe
    .update(req.body, {
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Cập nhật dữ liệu thành công!",
        });
      } else {
        res.send({
          message: `Lỗi không tìm thấy dữ liệu!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Xóa tài xế
exports.postXoaTaiXe = (req, res) => {
  const id = req.params.id;

  taixe
    .destroy({
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Đã xóa tài xế!",
        });
      } else {
        res.send({
          message: `Có lỗi, vui lòng liên hệ quản trị viên!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Có lỗi, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy danh sách phương tiện theo tài xế
exports.postListPhuongTienByTaiXe = async (req, res) => {
  const taxeId = req.body.id;
  try {
    const [data, metadata] = await sequelize.query(
      `SELECT
      a.id,
      b.bien_so,
      b.hovaten_chuhientai,
      b.sodienthoai_chuhientai,
      c.ten_loai, b.id as phuongtienvantaiId
    FROM
      taixe AS a
      JOIN phuongtienvantai AS b ON a.id = b.taixeId
      LEFT JOIN loaiphuongtienvantai AS c ON b.loaiphuongtienvantaiId = c.id
    WHERE a.id = ` + taxeId
    );
    if (data) {
      res.status(200).send(data);
    } else {
      res.status(500).send({ msg: "Lỗi trong quá trình thực hiện" });
    }
  } catch (error) {
    res.status(500).send({ msg: "Lỗi trong quá trình thực hiện" });
  }
};

exports.postDeletTaiXeGanPhuongTien = async (req, res) => {
  const ptvtId = req.body.id;
  try {
    const [data, metadata] = await sequelize.query(
      `UPDATE phuongtienvantai AS a
        SET a.taixeId = null
        WHERE a.id = ` + ptvtId
    );
    if (data) {
      res.status(200).send({
        message: "Cập nhật dữ liệu thành công!",
      });
    } else {
      res.status(500).send({ msg: "Lỗi trong quá trình thực hiện" });
    }
  } catch (error) {
    res.status(500).send({ msg: "Lỗi trong quá trình thực hiện" });
  }
};

exports.postGanTaiXe = async (req, res) => {
  const ptvtId = req.body.ptvtId;
  const taixeId = req.body.taixeId;
  try {
    const [data, metadata] = await sequelize.query(
      `UPDATE phuongtienvantai AS a
        SET a.taixeId = '` + taixeId +`'
        WHERE a.id = ` + ptvtId
    );
    if (data) {
      res.status(200).send({
        message: "Cập nhật dữ liệu thành công!",
      });
    } else {
      res.status(500).send({ msg: "Lỗi trong quá trình thực hiện" });
    }
  } catch (error) {
    res.status(500).send({ msg: "Lỗi trong quá trình thực hiện" });
  }
};
