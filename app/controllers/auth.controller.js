const db = require("../models");
const config = require("../config/auth.config");
const { user: User, role: Role, refreshToken: RefreshToken } = db;
const { userModel, user_roles } = require("../models");

const Op = db.Sequelize.Op;

const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

exports.signup = async (req, res) => {
  // Save User to Database
  try {
    const user = await User.create({
      ho_va_ten: req.body.ho_va_ten,
      username: req.body.username,
      trang_thai: req.body.trang_thai,
      email: req.body.email,
      password: bcrypt.hashSync(req.body.password, 8),
      unitID: "1",
    });

    // if (req.body.roles) {
    //   const roles = await Role.findAll({
    //     where: {
    //       name: {
    //         [Op.or]: req.body.roles,
    //       },
    //     },
    //   });

    // const result = user.setRoles(roles);
    if (user) {
      res.send({ message: "Tạo người dùng thành công!" });
    } else {
      // user has role = 1
      // const result = user.setRoles([2]);
      // if (result)
      res.send({ message: "Tạo người dùng thất bại!" });
    }
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

exports.signin = async (req, res) => {
  try {
    const user = await User.findOne({
      where: {
        username: req.body.username,
      },
    });
    

    if (!user) {
      return res.status(404).send({ message: "Không tìm thấy người dùng!" });
    }

    const passwordIsValid = bcrypt.compareSync(
      req.body.password,
      user.password
    );

    if (!passwordIsValid) {
      return res.status(401).send({
        message: "Sai mật khẩu đăng nhập!",
      });
    }

    const token = jwt.sign({ id: user.id }, config.secret, {
      algorithm: "HS256",
      allowInsecureKeySizes: true,
      expiresIn: 86400, // 24 hours
    });

    let authorities = [];

    const checkUserRole = await user_roles.findOne({ where: { id: user.id } });

    if (!checkUserRole) {
      return res.status(500).send({
        message: "Bạn chưa được phân quyền, vui lòng liên hệ quản trị viên",
      }); // Handle user not found scenario
    } else {
      const roles = await user.getRoles();

      for (let i = 0; i < roles.length; i++) {
        authorities.push("ROLE_" + roles[i].name.toUpperCase());
      }
      req.session.token = token;

      return res.status(200).send({
        ho_va_ten: user.ho_va_ten,
        username: user.username,
        email: user.email,
        roles: authorities,
      });
    }
  } catch (error) {
    return res.status(500).send({ message: error.message });
  }
};

// exports.signin = (req, res) => {
//   User.findOne({
//     where: {
//       username: req.body.username,
//     },
//   })
//     .then(async (user) => {
//       if (!user) {
//         return res.status(404).send({ message: "User Not found." });
//       }

//       const passwordIsValid = bcrypt.compareSync(
//         req.body.password,
//         user.password
//       );

//       if (!passwordIsValid) {
//         return res.status(401).send({
//           accessToken: null,
//           message: "Invalid Password!",
//         });
//       }

//       const token = jwt.sign({ id: user.id }, config.secret, {
//         expiresIn: config.jwtExpiration,
//       });

//       let refreshToken = await RefreshToken.createToken(user);

//       let authorities = [];
//       user.getRoles().then((roles) => {
//         for (let i = 0; i < roles.length; i++) {
//           authorities.push("ROLE_" + roles[i].name.toUpperCase());
//         }

//         res.status(200).send({
//           id: user.id,
//           username: user.username,
//           email: user.email,
//           roles: authorities,
//           accessToken: token,
//           refreshToken: refreshToken,
//         });
//       });
//     })
//     .catch((err) => {
//       res.status(500).send({ message: err.message });
//     });
// };

exports.refreshToken = async (req, res) => {
  const { refreshToken: requestToken } = req.body;

  if (requestToken == null) {
    return res.status(403).json({ message: "Refresh Token is required!" });
  }

  try {
    let refreshToken = await RefreshToken.findOne({
      where: { token: requestToken },
    });

    console.log(refreshToken);

    if (!refreshToken) {
      res.status(403).json({ message: "Refresh token is not in database!" });
      return;
    }

    if (RefreshToken.verifyExpiration(refreshToken)) {
      RefreshToken.destroy({ where: { id: refreshToken.id } });

      res.status(403).json({
        message: "Refresh token was expired. Please make a new signin request",
      });
      return;
    }

    const user = await refreshToken.getUser();
    let newAccessToken = jwt.sign({ id: user.id }, config.secret, {
      expiresIn: config.jwtExpiration,
    });

    return res.status(200).json({
      accessToken: newAccessToken,
      refreshToken: refreshToken.token,
    });
  } catch (err) {
    return res.status(500).send({ message: err });
  }
};

exports.signout = async (req, res) => {
  try {
    req.session = null;
    return res.status(200).send({
      message: "Bạn đã đăng xuất!",
    });
  } catch (err) {
    this.next(err);
  }
};

exports.resetpassword = async (req, res) => {
  const id = req.params.id;

  try {
    User.update(
      { password: bcrypt.hashSync(req.body.passregen, 8) },
      {
        where: { id: id },
      }
    )
      .then((num) => {
        if (num == 1) {
          res.send({
            message: "Reset mật khẩu thành công!",
          });
        } else {
          res.send({
            message: `Lỗi không tìm thấy dữ liệu!`,
          });
        }
      })
      .catch((err) => {
        res.status(500).send({
          message: "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
        });
      });
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};
