const { loaiphuongtienvantai } = require("../models");
var fs = require("fs");

// Lấy tất cả các loại phương tiện
exports.getTatCaLoaiPhuongTien = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  loaiphuongtienvantai
    .findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy các loại phương tiện đang hoạt động
exports.getLoaiPhuongTien_Enabled = (req, res) => {
  loaiphuongtienvantai
    .findAll({ where: { trang_thai: "1" } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy thông tin loại phương tiện
exports.getThongTinLoaiPhuongTien = (req, res) => {
  loaiphuongtienvantai
    .findByPk(req.params.id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Thêm loại phương tiện mới
exports.postThemLoaiPhuongTien = async (req, res) => {
  try {
    const tx = await loaiphuongtienvantai.create({
      ma_loai: req.body.ma_loai,
      ten_loai: req.body.ten_loai,
      trang_thai: req.body.trang_thai,
    });
    if (tx) {
      res
        .status(200)
        .send({
          message: "Thêm loại phương tiện thành công!",
          loaiphuongtienvantai: tx,
        });
    } else {
      res.status(500).send({ message: error.message });
    }
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

// Sửa thông tin loại phương tiện
exports.putChinhSuaLoaiPhuongTien = (req, res) => {
  const id = req.params.id;

  loaiphuongtienvantai
    .update(req.body, {
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Cập nhật dữ liệu thành công!",
        });
      } else {
        res.send({
          message: `Lỗi không tìm thấy dữ liệu!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Xóa loại phương tiện
exports.postXoaLoaiPhuongTien = (req, res) => {
  const id = req.params.id;

  loaiphuongtienvantai
    .destroy({
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Đã xóa loại phương tiện!",
        });
      } else {
        res.send({
          message: `Có lỗi, vui lòng liên hệ quản trị viên!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Có lỗi, vui lòng liên hệ quản trị viên",
      });
    });
};
