const { log_tool, sequelize } = require("../models");
const axios = require('axios');
const cron = require('node-cron');
const token = '7351560204:AAEnUpUiOxksBMrUh-CVAjWxGhM61GkvRWs';
const chatId = '-4206785299';

// Lấy log tool, cùng ngày thì chỉ lấy log tool mới nhất
exports.getLayLichSuQuetDuLieu = async (req, res) => {
  try {
    const date = req.body.date ? `'${req.body.date}'` : 'NULL';

    // Tạo truy vấn để thiết lập biến 
    const setDateQuery = `SET @date = ${date};`;

    // Truy vấn chính để lấy dữ liệu
    const mainQuery = `
        SELECT
            p.bien_so,
            n.id AS id_ncc,
            n.ten_ncc,
            l.tai_khoan,
            t.mat_khau,
            n.website,
            l.trang_thai,
            l.message
        FROM
            log_tool l
        JOIN
            (
                SELECT
                    tai_khoan,
                    nccthietbigshtId,
                    MAX(tg_bat_dau) AS max_tg_bat_dau,
                    DATE(tg_bat_dau) AS date_only
                FROM
                    log_tool
                WHERE
                    @date IS NULL OR DATE(tg_bat_dau) = DATE(@date)
                GROUP BY
                    tai_khoan,
                    nccthietbigshtId,
                    DATE(tg_bat_dau)
            ) max_logs
        ON
            l.tai_khoan = max_logs.tai_khoan
            AND l.nccthietbigshtId = max_logs.nccthietbigshtId
            AND l.tg_bat_dau = max_logs.max_tg_bat_dau
            AND DATE(l.tg_bat_dau) = max_logs.date_only
        JOIN
            nccthietbigsht n
        ON
            l.nccthietbigshtId = n.id
        JOIN
            taikhoangsht t
        ON
            l.tai_khoan LIKE t.tai_khoan
            AND t.trang_thai = 1
        JOIN
            phuongtienvantai p
        ON
            t.phuongtienvantaiId = p.id
        WHERE
            @date IS NULL OR DATE(l.tg_bat_dau) = DATE(@date)
        ORDER BY
            l.tai_khoan, l.nccthietbigshtId, l.trang_thai, l.tg_bat_dau;
    `;

    // Thực hiện các truy vấn tuần tự
    await sequelize.query(setDateQuery);
    const data = await sequelize.query(mainQuery, { type: sequelize.QueryTypes.SELECT });

    res.status(200).send(data);
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Lấy danh sách tài khoản chạy tool bị lỗi
exports.getLayDanhSachTaiKhoanLoi = async (req, res) => {
  try {
    const nccId = req.body.nccId ? `'${req.body.nccId}'` : 'NULL';
    const date = req.body.date ? `'${req.body.date}'` : 'NULL';

    // Tạo truy vấn để thiết lập biến
    const setNccIdQuery = `SET @nccId = ${nccId};`;
    const setDateQuery = `SET @date = ${date};`;

    // Truy vấn chính để lấy dữ liệu
    const mainQuery = `
      SELECT DISTINCT
          l.tai_khoan,
          l.tg_bat_dau,
          l.trang_thai,
          t.mat_khau
      FROM 
          log_tool l
      JOIN 
          (
              SELECT 
                  tai_khoan, 
                  nccthietbigshtId, 
                  MAX(tg_bat_dau) as max_tg_bat_dau,
                  DATE(tg_bat_dau) as date_only
              FROM 
                  log_tool
              WHERE 
                  (@date IS NULL OR DATE(tg_bat_dau) = DATE(@date))
                  AND (@nccId IS NULL OR nccthietbigshtId = @nccId)
              GROUP BY 
                  tai_khoan, 
                  nccthietbigshtId, 
                  DATE(tg_bat_dau)
          ) max_logs 
      ON 
          l.tai_khoan = max_logs.tai_khoan 
          AND l.nccthietbigshtId = max_logs.nccthietbigshtId 
          AND l.tg_bat_dau = max_logs.max_tg_bat_dau
          AND DATE(l.tg_bat_dau) = max_logs.date_only
      JOIN 
          taikhoangsht t 
      ON 
          l.tai_khoan like t.tai_khoan
      WHERE 
          t.trang_thai = 1
          AND l.trang_thai = 0
          AND (@date IS NULL OR DATE(l.tg_bat_dau) = DATE(@date))
          AND (@nccId IS NULL OR l.nccthietbigshtId = @nccId)
          AND (@nccId IS NULL OR t.nccthietbigshtId = @nccId)
      ORDER BY 
          l.tai_khoan;
    `;

    // Thực hiện các truy vấn tuần tự
    await sequelize.query(setNccIdQuery);
    await sequelize.query(setDateQuery);
    const data = await sequelize.query(mainQuery, { type: sequelize.QueryTypes.SELECT });

    res.status(200).send(data);
  } catch (err) {
    console.error(err);
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

// Lấy log tool gửi Telegram
exports.getLayLichSuQuetDuLieu_SendTelegram = async (req, res) => {
    try {
        const date = req.body.date ? `'${req.body.date}'` : 'NULL';

        // Tạo truy vấn để thiết lập biến 
        const setDateQuery = `SET @date = ${date};`;
    
        // Truy vấn chính để lấy dữ liệu
        const mainQuery = `
            SELECT
                p.bien_so,
                n.id AS id_ncc,
                n.ten_ncc,
                l.tai_khoan,
                n.website,
                l.trang_thai,
                l.message
            FROM
                log_tool l
            JOIN
                (
                    SELECT
                        tai_khoan,
                        nccthietbigshtId,
                        MAX(tg_bat_dau) AS max_tg_bat_dau,
                        DATE(tg_bat_dau) AS date_only
                    FROM
                        log_tool
                    WHERE
                        @date IS NULL OR DATE(tg_bat_dau) = DATE(@date)
                    GROUP BY
                        tai_khoan,
                        nccthietbigshtId,
                        DATE(tg_bat_dau)
                ) max_logs
            ON
                l.tai_khoan = max_logs.tai_khoan
                AND l.nccthietbigshtId = max_logs.nccthietbigshtId
                AND l.tg_bat_dau = max_logs.max_tg_bat_dau
                AND DATE(l.tg_bat_dau) = max_logs.date_only
            JOIN
                nccthietbigsht n
            ON
                l.nccthietbigshtId = n.id
            JOIN
                taikhoangsht t
            ON
                l.tai_khoan LIKE t.tai_khoan
                AND t.trang_thai = 1
            JOIN
                phuongtienvantai p
            ON
                t.phuongtienvantaiId = p.id
            WHERE
                @date IS NULL OR DATE(l.tg_bat_dau) = DATE(@date)
            ORDER BY
                l.tai_khoan, l.nccthietbigshtId, l.trang_thai, l.tg_bat_dau;
        `;
    
        // Thực hiện các truy vấn tuần tự
        await sequelize.query(setDateQuery);
        const data = await sequelize.query(mainQuery, { type: sequelize.QueryTypes.SELECT });
  
        return data;
    } catch (err) {
        return [];
    }
};

// Hàm để nhóm và xử lý dữ liệu
const processData = (data) => {
    return data.reduce((acc, item) => {
        // Tạo một khóa duy nhất cho mỗi nhóm `tai_khoan` và `id_ncc`
        const key = `${item.tai_khoan}-${item.id_ncc}`;

        // Nếu nhóm chưa tồn tại, tạo mới
        if (!acc[key]) {
            acc[key] = {
                tai_khoan: item.tai_khoan,
                id_ncc: item.id_ncc,
                bien_so: [],
                trang_thai: item.trang_thai
            };
        }

        // Nối `bien_so` vào mảng
        acc[key].bien_so.push(item.bien_so);
        return acc;
    }, {});
};

// Hàm lấy ngày của dữ liệu
const getYesterdayFormatted = () => {
    const date = new Date();
    date.setDate(date.getDate() - 1); // Lùi lại một ngày

    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0'); // Tháng bắt đầu từ 0
    const day = String(date.getDate()).padStart(2, '0');

    return `${day}/${month}/${year}`;
};

// Hàm thông báo Telegram
async function ThongBaoTelegram(message) {
    const sendMessage = async (chatId, message) => {
        const url = `https://api.telegram.org/bot${token}/sendMessage`;        
        try {
          const response = await axios.post(url, {
            chat_id: chatId,
            text: message
          });          
          console.log('Thông báo Telegram thành công!');
        } catch (error) {
          console.log('Thông báo Telegram thất bại!');
        }
    };
    sendMessage(chatId, message);
}

// Lập lịch thông báo vào 07:30 AM mỗi ngày
cron.schedule('30 07 * * *', async () => {
    const currentDate = new Date().toISOString().split('T')[0]; // Lấy ngày hiện tại
    const data = await exports.getLayLichSuQuetDuLieu_SendTelegram({ body: { date: currentDate } });

    // Chuyển đổi kết quả thành mảng
    const result = Object.values(processData(data)).map(item => ({
        ...item,
        bien_so: item.bien_so.join(', ') // Nối các bien_so thành chuỗi
    }));

    // Tính tổng số lượng `trang_thai`
    const summary = result.reduce((acc, item) => {
        if (item.trang_thai === 0) {
            acc.trang_thai_0 += 1;
        } else if (item.trang_thai === 1) {
            acc.trang_thai_1 += 1;
        }
        return acc;
    }, { trang_thai_0: 0, trang_thai_1: 0 });

    const total = summary.trang_thai_0 + summary.trang_thai_1;
    const message = `Báo cáo lịch sử quét dữ liệu ngày ${getYesterdayFormatted()}:
                    \n+ Thành công: ${summary.trang_thai_1}/${total} tài khoản.
                    \n+ Thất bại: ${summary.trang_thai_0}/${total} tài khoản.`;

    await ThongBaoTelegram(message);
});

// Lấy danh sách tài khoản chạy subtool bị lỗi
exports.getLayDanhSachTaiKhoanLoi_Sub = async (req, res) => {
    try {
      const nccId = req.body.nccId ? `'${req.body.nccId}'` : 'NULL';
      const date = req.body.date ? `'${req.body.date}'` : 'NULL';
  
      // Tạo truy vấn để thiết lập biến
      const setNccIdQuery = `SET @nccId = ${nccId};`;
      const setDateQuery = `SET @date = ${date};`;
  
      // Truy vấn chính để lấy dữ liệu
      const mainQuery = `
        SELECT DISTINCT
            l.tai_khoan,
            l.tg_bat_dau,
            l.trang_thai,
            t.mat_khau
        FROM 
            log_sub_tool l
        JOIN 
            (
                SELECT 
                    tai_khoan, 
                    nccthietbigshtId, 
                    MAX(tg_bat_dau) as max_tg_bat_dau,
                    DATE(tg_bat_dau) as date_only
                FROM 
                    log_sub_tool
                WHERE 
                    (@date IS NULL OR DATE(tg_bat_dau) = DATE(@date))
                    AND (@nccId IS NULL OR nccthietbigshtId = @nccId)
                GROUP BY 
                    tai_khoan, 
                    nccthietbigshtId, 
                    DATE(tg_bat_dau)
            ) max_logs 
        ON 
            l.tai_khoan = max_logs.tai_khoan 
            AND l.nccthietbigshtId = max_logs.nccthietbigshtId 
            AND l.tg_bat_dau = max_logs.max_tg_bat_dau
            AND DATE(l.tg_bat_dau) = max_logs.date_only
        JOIN 
            taikhoangsht t 
        ON 
            l.tai_khoan like t.tai_khoan
        WHERE 
            t.trang_thai = 1
            AND l.trang_thai = 0
            AND (@date IS NULL OR DATE(l.tg_bat_dau) = DATE(@date))
            AND (@nccId IS NULL OR l.nccthietbigshtId = @nccId)
            AND (@nccId IS NULL OR t.nccthietbigshtId = @nccId)
        ORDER BY 
            l.tai_khoan;
      `;
  
      // Thực hiện các truy vấn tuần tự
      await sequelize.query(setNccIdQuery);
      await sequelize.query(setDateQuery);
      const data = await sequelize.query(mainQuery, { type: sequelize.QueryTypes.SELECT });
  
      res.status(200).send(data);
    } catch (err) {
      console.error(err);
      res.status(500).send({
        message: err.message || "Không tìm thấy dữ liệu!",
      });
    }
  };