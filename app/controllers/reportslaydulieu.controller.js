const axios = require("axios");
const { tao_bao_cao, chi_tiet_bctk, chi_tiet_qua_toc_do_gioi_han, nccthietbigsht, log_tool, tao_bao_cao_sub, log_sub_tool, sequelize } = require("../models");

function formatDate(dateString) {
  const timezoneOffset = 7; // UTC+7 for Vietnam
  const date = new Date(dateString);
  const dateVN = new Date(date.getTime() + timezoneOffset * 60 * 60 * 1000);
  // const date = new Date(dateString);
  return `${dateVN.getFullYear()}-${padZero(dateVN.getMonth() + 1)}-${padZero(dateVN.getDate())} ${padZero(dateVN.getHours())}:${padZero(dateVN.getMinutes())}:${padZero(dateVN.getSeconds())}`;
}

// Helper function to pad single digits with a leading zero
function padZero(num) {
  return num < 10 ? `0${num}` : num;
}

// Lấy dữ liệu báo cáo vi phạm quá tốc độ và lái xe liên tục quá 4 tiếng
exports.getLayDuLieuThongKeBaoCao = async (req, res) => {
  const transaction = await sequelize.transaction();
  try {
    const { carPlateData, detailData, qtdghData, fromDate, toDate } = req.body;

    if (carPlateData && Object.keys(carPlateData).length > 0) {
      for (const carPlate in carPlateData) {
        const data = carPlateData[carPlate];
        await tao_bao_cao.create({
          tg_lay_bao_cao: formatDate(new Date().toISOString()), // Ngày hiện tại
          tg_bat_dau: formatDate(fromDate),
          tg_ket_thuc: formatDate(toDate),
          vipham_laixelientuc: data.vipham_laixelientuc,
          bien_so_xe: data.bien_so_xe,
          vipham_tocdo_5den10: data.vipham_tocdo_5den10,
          vipham_tocdo_10den20: data.vipham_tocdo_10den20,
          vipham_tocdo_20den35: data.vipham_tocdo_20den35,
          vipham_tocdo_tren35: data.vipham_tocdo_tren35,
          so_km: data.so_km,
          nccthietbigshtId: data.nccthietbigshtId
        }, { transaction });
      }
    }

    if (detailData && detailData.length > 0) {
      await Promise.all(detailData.map(async (detail) => {
        await chi_tiet_bctk.create({
          bien_so_xe: detail.bien_so_xe,
          ho_ten_lai_xe: detail.ho_ten_lai_xe,
          tg_bat_dau: formatDate(detail.tg_bat_dau),
          dia_diem_bat_dau: detail.dia_diem_bat_dau,
          tg_ket_thuc: formatDate(detail.tg_ket_thuc),
          dia_diem_ket_thuc: detail.dia_diem_ket_thuc,
          tg_lai_xe: detail.tg_lai_xe,
          so_km: detail.so_km,
          nccthietbigshtId: detail.nccthietbigshtId
        }, { transaction });
      }));
    }

    if (qtdghData && qtdghData.length > 0) {
      await Promise.all(qtdghData.map(async (detail) => {
        await chi_tiet_qua_toc_do_gioi_han.create({
          bien_so_xe: detail.bien_so_xe,
          ho_ten_lai_xe: detail.ho_ten_lai_xe,
          thoi_diem: formatDate(detail.thoi_diem),
          dia_diem: detail.dia_diem,
          toc_do_trung_binh: detail.toc_do_trung_binh,
          toc_do_gioi_han: detail.toc_do_gioi_han,
          nccthietbigshtId: detail.nccthietbigshtId
        }, { transaction });
      }));
    }

    // Commit nếu tất cả các thao tác thành công
    await transaction.commit();
    res.status(200).send({
      message: "Lưu dữ liệu thành công!"
    });
  } catch (error) {
    // Rollback nếu có lỗi xảy ra
    await transaction.rollback();
    console.error("Error:", error.message);
    res.status(500).send({ message: error.message });
  }
};

// Lấy tất cả các nhà cung cấp
exports.getTatCaNCCThietBiGSHT = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  nccthietbigsht
      .findAll({
          where: condition,
          attributes: ["id", "website"]
      })
      .then((data) => {
          res.status(200).send(data);
      })
      .catch((err) => {
          res.status(500).send({
              message: err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
          });
      });
};

// Lưu log tool
exports.saveLogTool = async (req, res) => {
  try {
    const { log } = req.body;

    await log_tool.create({
      tg_bat_dau: formatDate(log.tg_bat_dau),
      tg_ket_thuc: formatDate(log.tg_ket_thuc),
      tai_khoan: log.tai_khoan,
      nccthietbigshtId: log.nccthietbigshtId,
      trang_thai: log.trang_thai,
      message: log.message
    });

    res.status(200).send({
      message: "Lưu log thành công!"
    });
  } catch (error) {
    console.error("Error:", error.message);
    res.status(500).send({ message: error.message });
  }
};

// Lấy dữ liệu so_km từ tháng 1 đến tháng 7 năm 2024
exports.getLayDuLieuThongKeBaoCao_Sub = async (req, res) => {
  try {
    const { carPlateData, fromDate, toDate } = req.body;

    if (carPlateData !== undefined && Object.keys(carPlateData).length > 0) {
      for (const carPlate in carPlateData) {
        const data = carPlateData[carPlate];
        await tao_bao_cao_sub.create({
          tg_lay_bao_cao: formatDate(new Date().toISOString()), // Current date and time
          tg_bat_dau: formatDate(fromDate),
          tg_ket_thuc: formatDate(toDate),
          bien_so_xe: data.bien_so_xe,
          so_km: data.so_km,
          nccthietbigshtId: data.nccthietbigshtId
        });
      }
    }

    res.status(200).send({
      message: "Lưu dữ liệu thành công!"
    });
  } catch (error) {
    console.error("Error:", error.message);
    res.status(500).send({ message: error.message });
  }
};

// Lưu log subtool
exports.saveLogSubTool = async (req, res) => {
  try {
    const { log } = req.body;

    await log_sub_tool.create({
      tg_bat_dau: formatDate(log.tg_bat_dau),
      tg_ket_thuc: formatDate(log.tg_ket_thuc),
      tai_khoan: log.tai_khoan,
      nccthietbigshtId: log.nccthietbigshtId,
      trang_thai: log.trang_thai,
      message: log.message
    });

    res.status(200).send({
      message: "Lưu log thành công!"
    });
  } catch (error) {
    console.error("Error:", error.message);
    res.status(500).send({ message: error.message });
  }
};