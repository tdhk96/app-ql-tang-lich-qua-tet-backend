const {
  tao_bao_cao,
  nccthietbigsht,
  chi_tiet_bctk,
  phuongtienvantai,
  loaiphuongtienvantai
} = require("../models");
const { sequelize } = require("../models");
const { Op, literal } = require("sequelize");
const moment = require("moment");

// Lấy thống kê tất cả các nhà cung cấp dựa theo tháng/năm truyền vào
exports.getTatCaDuLieuTKNCC = async (req, res) => {
  const { monthAndYear, loaiViPham } = req.body;

  if (!monthAndYear) {
    res.status(400).send({
      message: "Vui lòng chọn một tháng và năm để lọc dữ liệu.",
    });
    return;
  }

  try {
    const currentMonth = moment().format("MM/YYYY");
    const selectedMonth = moment(monthAndYear, "MM/YYYY").format("MM/YYYY");

    let startOfMonth, endOfMonth;

    if (currentMonth === selectedMonth) {
      startOfMonth = moment().startOf("month").format("YYYY-MM-DD");
      endOfMonth = moment().format("YYYY-MM-DD");
    } else {
      startOfMonth = moment(monthAndYear, "MM/YYYY")
        .startOf("month")
        .format("YYYY-MM-DD");
      endOfMonth = moment(monthAndYear, "MM/YYYY")
        .endOf("month")
        .format("YYYY-MM-DD");
    }

    let filteredData = {
      tg_bat_dau: {
        [Op.between]: [startOfMonth, endOfMonth],
      },
    };

    if (loaiViPham === "vi phạm lái xe liên tục") {
      filteredData.vipham_laixelientuc = { [Op.gt]: 0 };
    } else if (loaiViPham === "vi phạm tốc độ") {
      filteredData[Op.or] = [
        { vipham_tocdo_5den10: { [Op.gt]: 0 } },
        { vipham_tocdo_10den20: { [Op.gt]: 0 } },
        { vipham_tocdo_20den35: { [Op.gt]: 0 } },
        { vipham_tocdo_tren35: { [Op.gt]: 0 } },
      ];
    }

    const data = await tao_bao_cao.findAll({
      where: filteredData,
      attributes: [
        [
          sequelize.fn(
            "MAX",
            sequelize.fn(
              "DATE_FORMAT",
              sequelize.col("tg_bat_dau"),
              "%d/%m/%Y %H:%i:%s"
            )
          ),
          "tg_bat_dau",
        ],
        [
          sequelize.fn(
            "MAX",
            sequelize.fn(
              "DATE_FORMAT",
              sequelize.col("tg_lay_bao_cao"),
              "%d/%m/%Y %H:%i:%s"
            )
          ),
          "tg_lay_bao_cao",
        ],
        [
          sequelize.fn(
            "MAX",
            sequelize.fn(
              "DATE_FORMAT",
              sequelize.col("tg_ket_thuc"),
              "%d/%m/%Y %H:%i:%s"
            )
          ),
          "tg_ket_thuc",
        ],
        [
          sequelize.fn("GROUP_CONCAT", sequelize.col("nccthietbigshtId")),
          "nccthietbigshtIds",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_laixelientuc")),
          "vipham_laixelientuc",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_tocdo_5den10")),
          "vipham_tocdo_5den10",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_tocdo_10den20")),
          "vipham_tocdo_10den20",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_tocdo_20den35")),
          "vipham_tocdo_20den35",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_tocdo_tren35")),
          "vipham_tocdo_tren35",
        ],
        [
          sequelize.fn("GROUP_CONCAT", sequelize.col("nccthietbigsht.website")),
          "ten_du_lieu_websites",
        ],
      ],
      group: ["tg_bat_dau"],
      include: [
        {
          model: nccthietbigsht,
          attributes: [],
        },
      ],
      order: [["tg_bat_dau", "DESC"]],
    });

    if (data.length === 0) {
      if (loaiViPham === "vi phạm lái xe liên tục") {
        res.status(404).send({
          message: "Không có xe vi phạm lái xe liên tục nào.",
        });
      } else if (loaiViPham === "vi phạm tốc độ") {
        res.status(404).send({
          message: "Không có xe vi phạm tốc độ nào.",
        });
      } else {
        res.status(404).send({
          message: "Dữ liệu chưa có cho tháng/năm đã chọn.",
        });
      }
      return;
    }

    res.send(data);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
    });
  }
};

exports.getExcelBaoCaoXeViPham = async (req, res) => {
  const { monthAndYear, loaiViPham } = req.body;

  if (!monthAndYear) {
    res.status(400).send({
      message: "Vui lòng chọn một tháng và năm để lọc dữ liệu.",
    });
    return;
  }

  try {
    const currentMonth = moment().format("MM/YYYY");
    const selectedMonth = moment(monthAndYear, "MM/YYYY").format("MM/YYYY");

    let startOfMonth, endOfMonth;

    if (currentMonth === selectedMonth) {
      startOfMonth = moment().startOf("month").format("YYYY-MM-DD");
      endOfMonth = moment().format("YYYY-MM-DD");
    } else {
      startOfMonth = moment(monthAndYear, "MM/YYYY")
        .startOf("month")
        .format("YYYY-MM-DD");
      endOfMonth = moment(monthAndYear, "MM/YYYY")
        .endOf("month")
        .format("YYYY-MM-DD");
    }

    let filteredData = {
      tg_bat_dau: {
        [Op.between]: [startOfMonth, endOfMonth],
      },
      [Op.or]: [
        { vipham_laixelientuc: { [Op.gt]: 0 } },
        { vipham_tocdo_5den10: { [Op.gt]: 0 } },
        { vipham_tocdo_10den20: { [Op.gt]: 0 } },
        { vipham_tocdo_20den35: { [Op.gt]: 0 } },
        { vipham_tocdo_tren35: { [Op.gt]: 0 } },
      ],
    };

    if (loaiViPham) {
      if (loaiViPham === "vi phạm lái xe liên tục") {
        filteredData.vipham_laixelientuc = { [Op.gt]: 0 };
      } else if (loaiViPham === "vi phạm tốc độ") {
        filteredData[Op.or] = [
          { vipham_tocdo_5den10: { [Op.gt]: 0 } },
          { vipham_tocdo_10den20: { [Op.gt]: 0 } },
          { vipham_tocdo_20den35: { [Op.gt]: 0 } },
          { vipham_tocdo_tren35: { [Op.gt]: 0 } },
        ];
      } else {
        res.status(400).send({
          message: "Loại vi phạm không hợp lệ.",
        });
        return;
      }
    }

    const data = await tao_bao_cao.findAll({
      where: filteredData,
      attributes: [
        [
          sequelize.fn(
            "DATE_FORMAT",
            sequelize.col("tg_bat_dau"),
            "%d/%m/%Y %H:%i:%s"
          ),
          "tg_bat_dau",
        ],
        [
          sequelize.fn(
            "DATE_FORMAT",
            sequelize.col("tg_lay_bao_cao"),
            "%d/%m/%Y %H:%i:%s"
          ),
          "tg_lay_bao_cao",
        ],
        "bien_so_xe",
        "vipham_laixelientuc",
        "vipham_tocdo_5den10",
        "vipham_tocdo_10den20",
        "vipham_tocdo_20den35",
        "vipham_tocdo_tren35",
        [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_websites"],
      ],
      include: [
        {
          model: nccthietbigsht,
          attributes: [],
        },
      ],
      order: [["tg_bat_dau", "DESC"]],
    });

    if (data.length === 0) {
      res.status(404).send({
        message: loaiViPham
          ? `Không có xe ${loaiViPham} nào.`
          : "Dữ liệu chưa có cho tháng/năm đã chọn.",
      });
      return;
    }

    res.send(data);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
    });
  }
};

// Lấy chi tiết tất cả các nhà cung cấp
exports.getTatCaDuLieuChiTietNCC = async (req, res) => {
  const title = req.query.title;
  const condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  try {
    const data = await tao_bao_cao.findAll({
      where: condition,
      attributes: [
        [
          sequelize.literal("DATE_FORMAT(tg_bat_dau, '%d/%m/%Y')"),
          "tg_bat_dau",
        ],
        [
          sequelize.literal("DATE_FORMAT(tg_lay_bao_cao, '%d/%m/%Y')"),
          "tg_lay_bao_cao",
        ],
        [
          sequelize.literal("DATE_FORMAT(tg_ket_thuc, '%d/%m/%Y')"),
          "tg_ket_thuc",
        ],
        "nccthietbigshtId",
        [
          sequelize.fn("sum", sequelize.col("vipham_laixelientuc")),
          "vipham_laixelientuc",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_tocdo_5den10")),
          "vipham_tocdo_5den10",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_tocdo_10den20")),
          "vipham_tocdo_10den20",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_tocdo_20den35")),
          "vipham_tocdo_20den35",
        ],
        [
          sequelize.fn("sum", sequelize.col("vipham_tocdo_tren35")),
          "vipham_tocdo_tren35",
        ],
        [
          sequelize.fn("GROUP_CONCAT", sequelize.col("bien_so_xe")),
          "bien_so_xe",
        ], // Gộp các biển số xe thành một mảng
        [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"], // Lấy dữ liệu trường website từ bảng nccthietbigsht
      ],
      group: [
        "tg_bat_dau",
        "tg_lay_bao_cao",
        "tg_ket_thuc",
        "nccthietbigshtId",
      ],
      include: [
        {
          model: nccthietbigsht,
          attributes: ["website"],
        },
      ],
    });

    res.send(data);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
    });
  }
};

// Lấy tất cả thống kê chi tiết thời gian lái xe liên tục
exports.getTatCaChiTietTKNCC = async (req, res) => {
  const { tg_bat_dau, tg_ket_thuc, nccthietbigshtId } = req.body;
  // Chuyển đổi thời gian đầu vào sang múi giờ Việt Nam (UTC+7)
  const vn_tg_bat_dau = moment(tg_bat_dau).utcOffset(7);
  const vn_tg_ket_thuc = moment(tg_ket_thuc).utcOffset(7);

  try {
    const data = await chi_tiet_bctk.findAll({
      where: {
        tg_bat_dau: {
          [Op.between]: [vn_tg_bat_dau, vn_tg_ket_thuc],
        },
        nccthietbigshtId: nccthietbigshtId,
      },
      attributes: [
        "id",
        "bien_so_xe",
        "ho_ten_lai_xe",
        [
          sequelize.literal("DATE_FORMAT(tg_bat_dau, '%d/%m/%Y %H:%i:%s')"),
          "tg_bat_dau",
        ],
        "dia_diem_bat_dau",
        [
          sequelize.literal("DATE_FORMAT(tg_ket_thuc, '%d/%m/%Y %H:%i:%s')"),
          "tg_ket_thuc",
        ],
        "dia_diem_ket_thuc",
        "tg_lai_xe",
        "so_km",
        "nccthietbigshtId",
      ],
    });

    res.send(data);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
    });
  }
};

// Lấy thống kê chi tiết bảng report
// exports.getBCViPhamTuNgayDenNgay = async (req, res) => {
//   const { tg_bat_dau, tg_ket_thuc } = req.body;
//   // Chuyển đổi thời gian đầu vào sang múi giờ Việt Nam (UTC+7)
//   const vn_tg_bat_dau = moment(tg_bat_dau)
//     .utcOffset(7)
//     .format("YYYY-MM-DD HH:mm:ss");
//   const vn_tg_ket_thuc = moment(tg_ket_thuc)
//     .utcOffset(7)
//     .format("YYYY-MM-DD HH:mm:ss");

//   try {
//     const data = await chi_tiet_bctk.findAll({
//       where: {
//         tg_bat_dau: {
//           [Op.between]: [vn_tg_bat_dau, vn_tg_ket_thuc],
//         },
//       },
//       attributes: [
//         "id",
//         "bien_so_xe",
//         [
//           sequelize.literal("DATE_FORMAT(tg_bat_dau, '%d/%m/%Y %H:%i:%s')"),
//           "tg_bat_dau",
//         ],
//         [
//           sequelize.literal("DATE_FORMAT(tg_ket_thuc, '%d/%m/%Y %H:%i:%s')"),
//           "tg_ket_thuc",
//         ],
//       ],
//       include: [
//         {
//           model: phuongtienvantai,
//           as: "phuongtien",
//           attributes: [
//             "hovaten_nguoidangky",
//             "hovaten_chuhientai",
//             "sodienthoai_chuhientai",
//           ],
//           where: {
//             bien_so: sequelize.col("chi_tiet_bctk.bien_so_xe"),
//           },
//           required: true,
//         },
//       ],
//     });

//     res.send(data);
//   } catch (error) {
//     res.status(500).send({
//       message:
//         error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
//     });
//   }
// };

// Lấy thống kê chi tiết bảng report
exports.getBCViPhamTuNgayDenNgay = async (req, res) => {
  const { tg_bat_dau, tg_ket_thuc, ten_loai, bien_so } = req.body;

  // Chuyển đổi thời gian đầu vào sang múi giờ Việt Nam (UTC+7)
  const vn_tg_bat_dau = moment(tg_bat_dau).utcOffset(7).format("YYYY-MM-DD");
  const vn_tg_ket_thuc = moment(tg_ket_thuc).utcOffset(7).format("YYYY-MM-DD");

  try {
    let query = `
      SELECT DISTINCT
          a.bien_so_xe, 
          b.hovaten_nguoidangky, 
          b.hovaten_chuhientai, 
          b.sodienthoai_chuhientai, 
          a.tg_bat_dau, 
          a.dia_diem_bat_dau, 
          a.tg_ket_thuc, 
          a.dia_diem_ket_thuc, 
          ROUND(a.tg_lai_xe, 0) AS tg_lai_xe
      FROM chi_tiet_bctk AS a
      INNER JOIN phuongtienvantai AS b ON a.bien_so_xe = b.bien_so
      INNER JOIN loaiphuongtienvantai AS c ON b.loaiphuongtienvantaiId = c.id
      WHERE b.trang_thai != '0' AND DATE(a.tg_bat_dau) BETWEEN '` + vn_tg_bat_dau + `' AND '` + vn_tg_ket_thuc + `'`;

    // Thêm điều kiện lọc theo `ten_loai` nếu `ten_loai` được truyền vào
    if (ten_loai) {
      query += ` AND c.id = '` + ten_loai + `'`;
    }

    // Thêm điều kiện lọc theo `bien_so` nếu `bien_so` là một mảng
    if (Array.isArray(bien_so) && bien_so.length > 0) {
      const bienSoList = bien_so.join(', '); // Giả sử bien_so là mảng số nguyên
      query += ` AND b.id IN (${bienSoList})`; // Sử dụng b.id thay vì b.bien_so
    }

    query += `
      ORDER BY a.bien_so_xe, a.tg_bat_dau;`;

    const data = await sequelize.query(query);

    res.send(data[0]);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
    });
  }
};

// Lấy thống kê chi tiết bảng chi_tiet_bctk
// exports.getBCChiTietViPhamTocDoTuNgayDenNgay = async (req, res) => {
//   const { tg_bat_dau, tg_ket_thuc } = req.body;

//   // Chuyển đổi thời gian đầu vào sang múi giờ Việt Nam (UTC+7)
//   const vn_tg_bat_dau = moment(tg_bat_dau)
//     .utcOffset(7)
//     .format("YYYY-MM-DD HH:mm:ss");
//   const vn_tg_ket_thuc = moment(tg_ket_thuc)
//     .utcOffset(7)
//     .format("YYYY-MM-DD HH:mm:ss");

//   try {
//     const data = await tao_bao_cao.findAll({
//       where: {
//         tg_bat_dau: {
//           [Op.between]: [vn_tg_bat_dau, vn_tg_ket_thuc],
//         },
//       },
//       attributes: [
//         "id",
//         "bien_so_xe",
//         [
//           sequelize.literal("DATE_FORMAT(tg_lay_bao_cao, '%d/%m/%Y %H:%i:%s')"),
//           "tg_lay_bao_cao",
//         ],
//         [
//           sequelize.literal("DATE_FORMAT(tg_bat_dau, '%d/%m/%Y %H:%i:%s')"),
//           "tg_bat_dau",
//         ],
//         "vipham_tocdo_5den10",
//         "vipham_tocdo_10den20",
//         "vipham_tocdo_20den35",
//         "vipham_tocdo_tren35",
//         [
//           sequelize.literal("DATE_FORMAT(tg_ket_thuc, '%d/%m/%Y %H:%i:%s')"),
//           "tg_ket_thuc",
//         ],
//         // [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
//       ],
//       // include: [
//       //   {
//       //     model: nccthietbigsht,
//       //     attributes: ["website"],
//       //   },
//       // ],
//       include: [
//         {
//           model: phuongtienvantai,
//           as: "phuongtien",
//           attributes: [
//             "hovaten_nguoidangky",
//             "hovaten_chuhientai",
//             "sodienthoai_chuhientai",
//           ],
//           where: {
//             bien_so: sequelize.col("report.bien_so_xe"),
//           },
//           required: true,
//         },
//       ],
//     });

//     res.send(data);
//   } catch (error) {
//     res.status(500).send({
//       message:
//         error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
//     });
//   }
// };

exports.getBCChiTietViPhamTocDoTuNgayDenNgay = async (req, res) => {
  const { tg_bat_dau, tg_ket_thuc, ten_loai, bien_so } = req.body;

  // Chuyển đổi thời gian đầu vào sang múi giờ Việt Nam (UTC+7)
  const vn_tg_bat_dau = moment(tg_bat_dau).utcOffset(7).format("YYYY-MM-DD");
  const vn_tg_ket_thuc = moment(tg_ket_thuc).utcOffset(7).format("YYYY-MM-DD");

  try {
    let query = `
      SELECT 
          a.tg_bat_dau, 
          a.bien_so_xe, 
          b.hovaten_nguoidangky, 
          b.hovaten_chuhientai, 
          b.sodienthoai_chuhientai, 
          MAX(a.vipham_tocdo_5den10) AS vipham_tocdo_5den10, 
          MAX(a.vipham_tocdo_10den20) AS vipham_tocdo_10den20, 
          MAX(a.vipham_tocdo_20den35) AS vipham_tocdo_20den35, 
          MAX(a.vipham_tocdo_tren35) AS vipham_tocdo_tren35
      FROM report AS a
      INNER JOIN phuongtienvantai AS b ON a.bien_so_xe = b.bien_so
      INNER JOIN loaiphuongtienvantai AS c ON b.loaiphuongtienvantaiId = c.id
      WHERE b.trang_thai != '0' DATE(a.tg_bat_dau) BETWEEN '` + vn_tg_bat_dau + `' AND '` + vn_tg_ket_thuc + `'`;

    // Thêm điều kiện lọc theo `ten_loai` nếu `ten_loai` được truyền vào
    if (ten_loai) {
      query += ` AND c.id = '` + ten_loai + `'`;
    }

    // Thêm điều kiện lọc theo `bien_so` nếu `bien_so` là một mảng
    if (Array.isArray(bien_so) && bien_so.length > 0) {
      const bienSoList = bien_so.join(', '); // Giả sử bien_so là mảng số nguyên
      query += ` AND b.id IN (${bienSoList})`; // Sử dụng b.id thay vì b.bien_so
    }

    query += `
      AND (a.vipham_tocdo_5den10 > 0 OR a.vipham_tocdo_10den20 > 0 OR a.vipham_tocdo_20den35 > 0 OR a.vipham_tocdo_tren35 > 0)
      GROUP BY a.tg_bat_dau, a.bien_so_xe, b.hovaten_nguoidangky, b.hovaten_chuhientai, b.sodienthoai_chuhientai
      ORDER BY a.bien_so_xe, a.tg_bat_dau;`;

    const data = await sequelize.query(query);

    res.send(data[0]);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
    });
  }
};

function countConsecutiveDays(dates) {
  if (!dates || dates.length === 0) {
      return { count: 0, days: [] };
  }

  let count = 1;
  let consecutiveDays = [dates[dates.length - 1]];

  for (let i = dates.length - 2; i >= 0; i--) {
      const currentDate = moment(dates[i]);
      const previousDate = moment(dates[i + 1]);

      if (currentDate.add(1, 'days').isSame(previousDate, 'day')) {
          count++;
          consecutiveDays.push(dates[i]);
      } else {
          break;
      }
  }

  return { count, days: consecutiveDays.reverse() };
}

// Lấy thống kê báo cáo mất tín hiệu
exports.getBCMatTinHieu = async (req, res) => {
  const { monthAndYear, so_lan_mth_lien_tuc, loai_xe_vi_pham_mth } = req.body;

  if (!monthAndYear) {
      res.status(400).send({
          message: "Vui lòng chọn một tháng và năm để lọc dữ liệu.",
      });
      return;
  }

  try {
      const currentMonth = moment().format("MM/YYYY");
      const selectedMonth = moment(monthAndYear, "MM/YYYY").format("MM/YYYY");

      let startOfMonth, endOfMonth;

      if (currentMonth === selectedMonth) {
          startOfMonth = moment().startOf("month").format("YYYY-MM-DD");
          endOfMonth = moment().format("YYYY-MM-DD");
      } else {
          startOfMonth = moment(monthAndYear, "MM/YYYY")
              .startOf("month")
              .format("YYYY-MM-DD");
          endOfMonth = moment(monthAndYear, "MM/YYYY")
              .endOf("month")
              .format("YYYY-MM-DD");
      }

      const vn_tg_bat_dau = startOfMonth + " 00:00:00";
      const vn_tg_ket_thuc = moment(endOfMonth + " 23:59:59").subtract(1, 'days').format("YYYY-MM-DD HH:mm:ss");

      // Tạo mảng ngày từ tg_bat_dau đến tg_ket_thuc
      const ngayViPhamTgTruyenVao = [];
      let currentDay = moment(vn_tg_bat_dau).utcOffset(7);
      const endDay = moment(vn_tg_ket_thuc).utcOffset(7);

      while (currentDay <= endDay) {
          ngayViPhamTgTruyenVao.push(currentDay.format("YYYY-MM-DD"));
          currentDay = currentDay.add(1, 'days');
      }

      // Lấy tất cả phương tiện vận tải kèm theo loại phương tiện
      const phuongTienVanTai = await phuongtienvantai.findAll({
          attributes: ["bien_so", "hovaten_nguoidangky", "hovaten_chuhientai", "sodienthoai_chuhientai", "loaiphuongtienvantaiId"],
          where: {
            trang_thai: { [Op.ne]: '0' } // Điều kiện trạng thái khác '0'
          },  
          include: [{
              model: loaiphuongtienvantai,
              attributes: ["ten_loai"],
          }]
      });

      // Tạo một Set để lưu trữ tất cả các biển số xe trong bảng phương tiện vận tải
      const bienSoSet = new Set(phuongTienVanTai.map(pt => pt.bien_so.replace(/\s+/g, '').toUpperCase()));

      // Lấy dữ liệu báo cáo trong khoảng thời gian, chỉ lấy các biển số xe trùng với bienSoSet
      const baoCao = await tao_bao_cao.findAll({
          where: {
              tg_bat_dau: {
                  [Op.between]: [vn_tg_bat_dau, vn_tg_ket_thuc],
              },
              bien_so_xe: {
                  [Op.in]: Array.from(bienSoSet),
              }
          },
          attributes: ["id", "bien_so_xe", "tg_bat_dau", "tg_ket_thuc", "so_km", "nccthietbigshtId"],
          include: [{
              model: nccthietbigsht,
              attributes: ["website"],
              as: 'nccthietbigsht'
          }]
      });

      // Khởi tạo biến lưu trữ dữ liệu vi phạm
      const viPhamMTHData = {};

      // Duyệt qua từng bản ghi báo cáo
      for (const bc of baoCao) {
          const bienSoXe = bc.bien_so_xe.replace(/\s+/g, '').toUpperCase();
          const tgBatDau = bc.tg_bat_dau;
          const soKm = bc.so_km;
          const website = bc.nccthietbigsht ? bc.nccthietbigsht.website : null;

          if (!viPhamMTHData[bienSoXe]) {
              viPhamMTHData[bienSoXe] = {
                  soLanViPhamMTH: 0,
                  ngayViPhamMTH: new Set(),
                  ten_du_lieu_websites: website,
              };
          }

          // Kiểm tra nếu biển số xe có so_km = 0
          if (soKm === 0) {
              const ngayViPhamMTH = moment(tgBatDau).format("YYYY-MM-DD");
              if (!viPhamMTHData[bienSoXe].ngayViPhamMTH.has(ngayViPhamMTH)) {
                  viPhamMTHData[bienSoXe].soLanViPhamMTH++;
                  viPhamMTHData[bienSoXe].ngayViPhamMTH.add(ngayViPhamMTH);
              }
          }
      }

      // Kiểm tra những ngày không có dữ liệu và xem xét chúng là vi phạm mất tín hiệu
      for (const bienSoXe of Array.from(bienSoSet)) {
          if (!viPhamMTHData[bienSoXe]) {
              viPhamMTHData[bienSoXe] = {
                  soLanViPhamMTH: 0,
                  ngayViPhamMTH: new Set(),
                  ten_du_lieu_websites: null,
              };
          }

          for (const ngay of ngayViPhamTgTruyenVao) {
              if (!viPhamMTHData[bienSoXe].ngayViPhamMTH.has(ngay) && !baoCao.some(bc => bc.bien_so_xe === bienSoXe && moment(bc.tg_bat_dau).format("YYYY-MM-DD") === ngay)) {
                  viPhamMTHData[bienSoXe].soLanViPhamMTH++;
                  viPhamMTHData[bienSoXe].ngayViPhamMTH.add(ngay);
              }
          }
      }

      // Chuyển đổi dữ liệu vi phạm sang định dạng mong muốn
      const result = [];
      for (const bienSoXe in viPhamMTHData) {
          const { soLanViPhamMTH, ngayViPhamMTH, ten_du_lieu_websites } = viPhamMTHData[bienSoXe];
          const ngayViPhamMTHArray = Array.from(ngayViPhamMTH);

          // So sánh với mảng ngày trong khoảng tg_bat_dau, tg_ket_thuc
          const ngayViPhamMTHThieu = ngayViPhamTgTruyenVao.filter(ngay => ngayViPhamMTHArray.includes(ngay));

          // Đếm số ngày vi phạm liên tục
          const { count: soLanViPhamMTHLienTuc, days: ngayViPhamMTHLienTuc } = countConsecutiveDays(ngayViPhamMTHThieu);

          // Tìm thông tin từ bảng phuongtienvantai
          const pt = phuongTienVanTai.find(pt => pt.bien_so.replace(/\s+/g, '').toUpperCase() === bienSoXe);
          const loaiPhuongTien = pt && pt.loaiphuongtienvantai ? pt.loaiphuongtienvantai.ten_loai : null;

          // Lọc dữ liệu dựa trên so_lan_mth_lien_tuc và loai_xe_vi_pham_mth nếu có truyền vào
          if (soLanViPhamMTHLienTuc > 0 &&
              (!so_lan_mth_lien_tuc || soLanViPhamMTHLienTuc === so_lan_mth_lien_tuc) &&
              (!loai_xe_vi_pham_mth || (pt && pt.loaiphuongtienvantaiId === loai_xe_vi_pham_mth))) {
              result.push({
                  bienSoXe,
                  soLanViPhamMTH,
                  ngayViPhamMTH: ngayViPhamMTHThieu,
                  soLanViPhamMTHLienTuc,
                  ngayViPhamMTHLienTuc,
                  hovaten_nguoidangky: pt ? pt.hovaten_nguoidangky : null,
                  hovaten_chuhientai: pt ? pt.hovaten_chuhientai : null,
                  sodienthoai_chuhientai: pt ? pt.sodienthoai_chuhientai : null,
                  ten_du_lieu_websites: ten_du_lieu_websites,
                  loaiPhuongTien
              });
          }
      }

      res.send(result);
  } catch (error) {
      res.status(500).send({
          message: error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên.",
      });
  }
};

// Lấy tất cả thống kê chi tiết tổng hợp
exports.getTatCaDuLieuChiTietTKNCC = async (req, res) => {
  const { tg_bat_dau, tg_ket_thuc, nccthietbigshtId } = req.body;

  // Chuyển đổi thời gian đầu vào sang múi giờ Việt Nam (UTC+7)
  const vn_tg_bat_dau = moment(tg_bat_dau)
    .utcOffset(7)
    .format("YYYY-MM-DD HH:mm:ss");
  const vn_tg_ket_thuc = moment(tg_ket_thuc)
    .utcOffset(7)
    .format("YYYY-MM-DD HH:mm:ss");

  try {
    const data = await tao_bao_cao.findAll({
      where: {
        tg_bat_dau: {
          [Op.between]: [vn_tg_bat_dau, vn_tg_ket_thuc],
        },
        nccthietbigshtId: nccthietbigshtId,
      },
      attributes: [
        "id",
        "bien_so_xe",
        "tg_lay_bao_cao",
        [
          sequelize.literal("DATE_FORMAT(tg_bat_dau, '%d/%m/%Y %H:%i:%s')"),
          "tg_bat_dau",
        ],
        "vipham_laixelientuc",
        "vipham_tocdo_5den10",
        "vipham_tocdo_10den20",
        "vipham_tocdo_20den35",
        "vipham_tocdo_tren35",
        [
          sequelize.literal("DATE_FORMAT(tg_ket_thuc, '%d/%m/%Y %H:%i:%s')"),
          "tg_ket_thuc",
        ],
        "nccthietbigshtId",
      ],
      include: [
        {
          model: nccthietbigsht,
          attributes: ["website"],
        },
      ],
    });

    res.send(data);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
    });
  }
};

// Xem chi tiết từng cột trên thống kê tổng quát
exports.getChiTietCotTKNCC = async (req, res) => {
  const { tg_bat_dau, tg_ket_thuc, column } = req.body;
  const vn_tg_bat_dau = moment(tg_bat_dau).utcOffset(7);
  const vn_tg_ket_thuc = moment(tg_ket_thuc).utcOffset(7);
  try {
    let attributes;
    let whereClause = {
      tg_bat_dau: {
        [Op.between]: [vn_tg_bat_dau, vn_tg_ket_thuc],
      },
    };

    // Xác định cột được chọn và truy vấn dữ liệu tương ứng từ bảng tao_bao_cao
    switch (parseInt(column)) {
      case 1:
        attributes = [
          "bien_so_xe",
          "tg_lay_bao_cao",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        break;
      case 2:
        attributes = [
          "bien_so_xe",
          "tg_bat_dau",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        break;
      case 3:
        attributes = [
          "bien_so_xe",
          "tg_ket_thuc",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        break;
      case 4:
        attributes = [
          "bien_so_xe",
          "vipham_laixelientuc",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        whereClause.vipham_laixelientuc = { [Op.gt]: 0 };
        break;
      case 5:
        attributes = [
          "bien_so_xe",
          "vipham_tocdo_5den10",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        whereClause.vipham_tocdo_5den10 = { [Op.gt]: 0 };
        break;
      case 6:
        attributes = [
          "bien_so_xe",
          "vipham_tocdo_10den20",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        whereClause.vipham_tocdo_10den20 = { [Op.gt]: 0 };
        break;
      case 7:
        attributes = [
          "bien_so_xe",
          "vipham_tocdo_20den35",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        whereClause.vipham_tocdo_20den35 = { [Op.gt]: 0 };
        break;
      case 8:
        attributes = [
          "bien_so_xe",
          "vipham_tocdo_tren35",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        whereClause.vipham_tocdo_tren35 = { [Op.gt]: 0 };
        break;
      case 9:
        attributes = [
          "bien_so_xe",
          "nccthietbigshtId",
          [sequelize.literal("nccthietbigsht.website"), "ten_du_lieu_website"],
        ];
        break;
      default:
        res.status(400).send({ message: "Column not found" });
        return;
    }

    const data = await tao_bao_cao.findAll({
      where: whereClause,
      attributes: attributes,
      include: [
        {
          model: nccthietbigsht,
          attributes: [],
        },
      ],
    });

    // Kiểm tra nếu không có dữ liệu tương ứng, trả về thông báo lỗi
    if (!data || data.length === 0) {
      res.status(404).send({ message: "Data not found" });
      return;
    }

    res.send(data);
  } catch (error) {
    res.status(500).send({
      message:
        error.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
    });
  }
};

// Thống kê mất tín hiệu
