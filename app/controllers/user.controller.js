const { user, role, user_roles } = require("../models");
const { Op } = require("sequelize");
const { sequelize } = require("../models");
var fs = require("fs");
const { log } = require("console");
// Lấy tất cả các người dùng
exports.getTatCaNguoiDung = (req, res) => {
  // const title = req.query.title;
  // var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  user
    .findAll(
      { where: { username: { [Op.not]: "superadmin" } } },
      { email: 1, ho_va_ten: 1, trang_thai: 1, username: 1 }
    )
    // .select({ email: 1, ho_va_ten: 1, trang_thai: 1, username: 1 })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy các người dùng đang hoạt động
exports.getNguoiDung_Enabled = (req, res) => {
  user
    .findAll({ where: { trang_thai: "1" } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy thông tin người dùng
exports.getThongTinNguoiDung = (req, res) => {
  user
    .findByPk(req.params.id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Sửa thông tin người dùng
exports.putChinhSuaNguoiDung = (req, res) => {
  const id = req.params.id;
  user
    .update(req.body, {
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Cập nhật dữ liệu thành công!",
        });
      } else {
        res.send({
          message: `Lỗi không tìm thấy dữ liệu!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Cập nhậ trạng thái tài khoản
exports.putCapNhatTrangThaiTaiKhoan = (req, res) => {
  const id = req.params.id;

  user
    .update(req.body, {
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Cập nhật dữ liệu thành công!",
        });
      } else {
        res.send({
          message: `Lỗi không tìm thấy dữ liệu!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Xóa người dùng
exports.postXoaNguoiDung = (req, res) => {
  const id = req.params.id;

  user
    .destroy({
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "Đã xóa người dùng!",
        });
      } else {
        res.send({
          message: `Có lỗi, vui lòng liên hệ quản trị viên!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Có lỗi, vui lòng liên hệ quản trị viên",
      });
    });
};

// Lấy danh sách phân quyền đang hoạt động
exports.getPhanQuyen_Enabled = (req, res) => {
  role
    .findAll({ where: { status: "1" } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Lỗi khi lấy dữ liệu, vui lòng liên hệ quản trị viên",
      });
    });
};

// Hàm thêm phân quyền cho người dùng
async function assignRolePermissions(userId, roleIds) {
  // Khởi tạo transaction
  const transaction = await sequelize.transaction();

  try {
    // Xóa các phân quyền hiện tại của người dùng
    await user_roles.destroy(
      { where: { userId: userId } },
      { transaction: transaction }
    );

    // Thêm các phân quyền mới cho người dùng
    userId = parseInt(userId);
    const userRoleData = roleIds.map((roleId) => ({ userId, roleId }));
    await user_roles.bulkCreate(userRoleData, { transaction: transaction });

    await transaction.commit();
    // console.log("Gán phân quyền thành công!");
  } catch (error) {
    await transaction.rollback();
    // console.error("Xảy ra lỗi khi gán phân quyền:", error);
  }
}

// Phân quyền
exports.postPhanQuyen = async (req, res) => {
  const id = req.params.id;
  const lstQuyen = req.body.listRoles;

  try {
    await assignRolePermissions(id, lstQuyen);

    res.status(200).send({ message: "Phân quyền người dùng thành công" });
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};

// Lấy danh sách phân quyền theo user
exports.getListQuyenNguoiDung = async (req, res) => {
  const id = req.params.id;

  try {
    let authorities = [];
    const lstUserRole = await user_roles.findAll({ where: { userId: id } });

    for (let i = 0; i < lstUserRole.length; i++) {
      authorities.push(lstUserRole[i].id);
    }
    res.status(200).send(authorities);
  } catch (error) {
    res.status(500).send({ message: error.message });
  }
};
