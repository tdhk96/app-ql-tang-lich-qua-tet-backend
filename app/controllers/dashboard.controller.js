const { phuongtienvantai, sequelize } = require("../models");
const moment = require("moment");

exports.getThongKePTVT = async (req, res) => {
  // Lấy ngày bắt đầu của tháng hiện tại
  const startOfMonth = moment().startOf("month").format("YYYY-MM-DD");

  // Lấy ngày cuối cùng của tháng hiện tại
  const endOfMonth = moment().endOf("month").format("YYYY-MM-DD");

  const data = await sequelize.query(
    `SELECT
        COUNT( CASE WHEN trang_thai = 1 THEN 1 END ) AS ptvt_hoatdong,
        COUNT( CASE WHEN createdAt >= '` +
      startOfMonth +
      `' AND createdAt <= '` +
      endOfMonth +
      `' THEN 1 END ) AS ptvt_moi_trong_thai 
      FROM
        phuongtienvantai;
  `
  );

  if (data) {
    res.send(data[0]);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

exports.getThongKeTaiXe = async (req, res) => {
  // Lấy ngày bắt đầu của tháng hiện tại
  const startOfMonth = moment().startOf("month").format("YYYY-MM-DD");

  // Lấy ngày cuối cùng của tháng hiện tại
  const endOfMonth = moment().endOf("month").format("YYYY-MM-DD");

  const data = await sequelize.query(
    `SELECT
        COUNT( CASE WHEN trang_thai = 1 THEN 1 END ) AS taixe_hoatdong,
        COUNT( CASE WHEN createdAt >= '` +
      startOfMonth +
      `' AND createdAt <= '` +
      endOfMonth +
      `' THEN 1 END ) AS taixe_moi_trong_thai 
      FROM
        taixe;
  `
  );

  if (data) {
    res.send(data[0]);
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

exports.getPhuongTienDenHanDangKiem = async (req, res) => {
  const [data, metadata] = await sequelize.query(
    `SELECT COUNT(*) AS countPTVTHH_DangKiem
      FROM phuongtienvantai
      WHERE dang_kiem IS NULL
        OR dang_kiem BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 60 DAY);`
  );

  if (data) {
    const count = data[0].countPTVTHH_DangKiem;
    res.json({ count });
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

exports.getPhuongTienDenHanBaoHiem = async (req, res) => {
  const [data, metadata] = await sequelize.query(
    `SELECT COUNT(*) AS countPTVTHH_BaoHiem
    FROM phuongtienvantai
    WHERE bao_hiem IS NULL
   OR bao_hiem BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 60 DAY);`
  );

  if (data) {
    const count = data[0].countPTVTHH_BaoHiem;
    res.json({ count });
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};

exports.getPhuongTienDenHanPhuHieu = async (req, res) => {
  const [data, metadata] = await sequelize.query(
    `SELECT COUNT(*) AS countPTVTHH_PhuHieu
      FROM phuongtienvantai
    WHERE ngay_het_han_phu_hieu IS NULL
   OR ngay_het_han_phu_hieu BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 60 DAY);`
  );

  if (data) {
    const count = data[0].countPTVTHH_PhuHieu;
    res.json({ count });
  } else {
    res.status(500).send({
      message: err.message || "Không tìm thấy dữ liệu!",
    });
  }
};
