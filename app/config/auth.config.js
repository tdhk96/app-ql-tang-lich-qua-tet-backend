module.exports = {
  secret: "phan-mem-quan-ly-van-tai",
  jwtExpiration: 3600, // 1 hour
  jwtRefreshExpiration: 86400, // 24 hours
};
